/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-vrmf-model)
 */
package org.fao.fi.comet.domain.vrmf.common;

import java.io.Serializable;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author ffiorellato
 */
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor @AllArgsConstructor @EqualsAndHashCode @ToString @Getter @Setter
public class MatchingProcessConfiguration implements Serializable {
	private static final long serialVersionUID = 6684326247420420378L;
	
	private @XmlAttribute int maxCandidatesPerEntry;
	private @XmlAttribute double minScore;
	
	private @XmlElement String[] sources;
	private @XmlElement Long sourcesUpdatedBefore;
	private @XmlElement Long sourcesUpdatedAfter;
	
	private @XmlElement String[] targets;
	private @XmlElement Long targetsUpdatedBefore;
	private @XmlElement Long targetsUpdatedAfter;
	
	@XmlElementWrapper(name="matchletConfigurations")
	@XmlElement(name="matchletConfiguration")
	private Collection<MatchletConfiguration> matchletConfigurations;
}
