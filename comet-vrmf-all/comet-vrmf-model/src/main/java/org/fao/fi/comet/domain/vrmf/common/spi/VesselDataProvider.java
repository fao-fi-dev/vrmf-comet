/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-vrmf-model)
 */
package org.fao.fi.comet.domain.vrmf.common.spi;

import java.util.Collection;
import java.util.Date;

import org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;

/**
 * The Interface VesselDataProvider.
 *
 * @author Fiorellato
 */
public interface VesselDataProvider extends SizeAwareDataProvider<ExtendedVessel> {
	
	/**
	 * Sets the updated from.
	 *
	 * @param date the new updated from
	 */
	void setUpdatedFrom(Date date);
	
	/**
	 * Sets the updated to.
	 *
	 * @param date the new updated to
	 */
	void setUpdatedTo(Date date);
	
	/**
	 * Sets the sources.
	 *
	 * @param sources the new sources
	 */
	void setSources(String[] sources);
	
	/**
	 * Sets the attributes.
	 *
	 * @param attributes the new attributes
	 */
	void setAttributes(Collection<Class<?>> attributes);
}
