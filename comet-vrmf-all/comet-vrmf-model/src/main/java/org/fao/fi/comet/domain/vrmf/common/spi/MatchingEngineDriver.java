/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-vrmf-model)
 */
package org.fao.fi.comet.domain.vrmf.common.spi;

import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus;
import org.fao.fi.comet.core.model.engine.MatchingEngineStatus;
import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;

/**
 * The Interface MatchingEngineDriver.
 */
public interface MatchingEngineDriver {
	
	/**
	 * Describe self.
	 *
	 * @return the string
	 */
	String describeSelf();
	
	/**
	 * Count sources.
	 *
	 * @param configuration the configuration
	 * @return the int
	 */
	int countSources(MatchingProcessConfiguration configuration);
	
	/**
	 * Count targets.
	 *
	 * @param configuration the configuration
	 * @return the int
	 */
	int countTargets(MatchingProcessConfiguration configuration);

	/**
	 * Gets the process status.
	 *
	 * @return the process status
	 */
	MatchingEngineProcessStatus getProcessStatus();

	/**
	 * Checks if is.
	 *
	 * @param status the status
	 * @return true, if successful
	 */
	boolean is(MatchingEngineStatus... status);
	
	/**
	 * Start process.
	 *
	 * @param configuration the configuration
	 * @return the matching engine process result
	 * @throws Exception the exception
	 */
	MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> startProcess(MatchingProcessConfiguration configuration) throws Exception;
	
	/**
	 * Halt process.
	 */
	void haltProcess();
	
	/**
	 * Gets the process configuration.
	 *
	 * @return the process configuration
	 */
	MatchingProcessConfiguration getProcessConfiguration();
	
	/**
	 * Dump.
	 *
	 * @return the matching engine process result
	 * @throws Exception the exception
	 */
	MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> dump() throws Exception;
	
	/**
	 * Gets the refresh time.
	 *
	 * @return the refresh time
	 */
	int getRefreshTime();
}
