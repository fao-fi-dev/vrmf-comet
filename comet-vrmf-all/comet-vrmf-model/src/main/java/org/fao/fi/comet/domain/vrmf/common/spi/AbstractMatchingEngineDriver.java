/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-vrmf-model)
 */
package org.fao.fi.comet.domain.vrmf.common.spi;

import org.fao.fi.comet.core.model.engine.MatchingEngineStatus;

/**
 * The Class AbstractMatchingEngineDriver.
 */
public abstract class AbstractMatchingEngineDriver implements MatchingEngineDriver {
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#is(org.fao.fi.comet.core.model.engine.MatchingEngineStatus[])
	 */
	@Override
	public boolean is(MatchingEngineStatus... status) {
		return this.getProcessStatus().is(status);
	}
}
