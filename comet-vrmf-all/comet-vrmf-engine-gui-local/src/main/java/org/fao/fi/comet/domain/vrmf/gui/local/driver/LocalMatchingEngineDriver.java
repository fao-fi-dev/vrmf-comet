/**
 * 
 */
package org.fao.fi.comet.domain.vrmf.gui.local.driver;

import java.net.Inet4Address;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler;
import org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler;
import org.fao.fi.comet.core.exceptions.MatchingProcessException;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessorInfo;
import org.fao.fi.comet.core.model.engine.MatchingsData;
import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletData;
import org.fao.fi.comet.core.patterns.data.partitioners.impl.IdentityDataPartitioner;
import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;
import org.fao.fi.comet.domain.vrmf.common.spi.AbstractMatchingEngineDriver;
import org.fao.fi.comet.domain.vrmf.common.spi.VesselDataProvider;
import org.fao.fi.comet.domain.vrmf.engine.VesselsMatchingEngine;
import org.fao.fi.comet.domain.vrmf.handlers.id.VesselIDHandler;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;
import org.springframework.context.annotation.Lazy;

/**
 * @author ffiorellato
 *
 */
@Lazy @Singleton @Named("matching.engine.driver.local")
public class LocalMatchingEngineDriver extends AbstractMatchingEngineDriver {
	private VesselsMatchingEngine _engine = new VesselsMatchingEngine();
	private MatchingEngineProcessConfiguration _engineConfiguration = new MatchingEngineProcessConfiguration();
	private MatchingProcessHandler<ExtendedVessel, ExtendedVessel> _handler = new SilentMatchingProcessHandler<ExtendedVessel, ExtendedVessel>(); 

	@Inject private VesselDataProvider _sourcesProvider;
	@Inject private VesselDataProvider _targetsProvider;

	private MatchingProcessConfiguration _configuration;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#describeSelf()
	 */
	@Override
	public String describeSelf() {
		String hostName = "localhost";
		
		try {
			hostName = Inet4Address.getLocalHost().getHostName();
		} catch (Throwable t) {
			//Suffocate
		}
		
		return hostName;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.driver.MatchingProcessDriver#getRefreshTime()
	 */
	@Override
	public int getRefreshTime() {
		return 50;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.driver.MatchingProcessDriver#getProcessConfiguration()
	 */
	@Override
	public MatchingProcessConfiguration getProcessConfiguration() {
		return this._configuration;
	}
	
	/**
	 * @return the sourcesProvider
	 */
	public VesselDataProvider getSourcesProvider() {
		return this._sourcesProvider;
	}

	/**
	 * @param sourcesProvider the sourcesProvider to set
	 */
	public void setSourcesProvider(VesselDataProvider sourcesProvider) {
		this._sourcesProvider = sourcesProvider;
	}

	/**
	 * @return the targetsProvider
	 */
	public VesselDataProvider getTargetsProvider() {
		return this._targetsProvider;
	}

	/**
	 * @param targetsProvider the targetsProvider to set
	 */
	public void setTargetsProvider(VesselDataProvider targetsProvider) {
		this._targetsProvider = targetsProvider;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.driver.MatchingProcessDriver#countSources(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public synchronized int countSources(MatchingProcessConfiguration configuration) {
		this._sourcesProvider.setUpdatedFrom(configuration.getSourcesUpdatedAfter() == null ? null : new Date(configuration.getSourcesUpdatedAfter()));
		this._sourcesProvider.setUpdatedTo(configuration.getSourcesUpdatedBefore() == null ? null : new Date(configuration.getSourcesUpdatedBefore()));
		this._sourcesProvider.setSources(configuration.getSources());
		this._sourcesProvider.setProviderID("SOURCES");
		
		return this._sourcesProvider.getAvailableDataSize();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.driver.MatchingProcessDriver#countTargets(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public synchronized int countTargets(MatchingProcessConfiguration configuration) {
		this._targetsProvider.setUpdatedFrom(configuration.getTargetsUpdatedAfter() == null ? null : new Date(configuration.getTargetsUpdatedAfter()));
		this._targetsProvider.setUpdatedTo(configuration.getTargetsUpdatedBefore() == null ? null : new Date(configuration.getTargetsUpdatedBefore()));
		this._targetsProvider.setSources(configuration.getTargets());
		this._targetsProvider.setProviderID("TARGETS");
		
		return this._targetsProvider.getAvailableDataSize();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.driver.MatchingProcessDriver#startProcess(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> startProcess(MatchingProcessConfiguration configuration) throws MatchingProcessException {
		this._handler = new SilentMatchingProcessHandler<ExtendedVessel, ExtendedVessel>(); 
		this._configuration = configuration;
		
		this._engineConfiguration = new MatchingEngineProcessConfiguration();
		this._engineConfiguration.setMinimumAllowedWeightedScore(configuration.getMinScore());
		this._engineConfiguration.setMaxCandidatesPerEntry(configuration.getMaxCandidatesPerEntry());
		this._engineConfiguration.setHandleErrors(false);
		this._engineConfiguration.setMatchletsConfiguration(configuration.getMatchletConfigurations());
		
		Collection<Class<?>> matchingAttributes = new HashSet<Class<?>>();
		
		this._targetsProvider.setUpdatedFrom(configuration.getTargetsUpdatedAfter() == null ? null : new Date(configuration.getTargetsUpdatedAfter()));
		this._targetsProvider.setUpdatedTo(configuration.getTargetsUpdatedBefore() == null ? null : new Date(configuration.getTargetsUpdatedBefore()));
		this._targetsProvider.setSources(configuration.getTargets());
		this._targetsProvider.setProviderID("TARGETS");
		
		this._sourcesProvider.setUpdatedFrom(configuration.getSourcesUpdatedAfter() == null ? null : new Date(configuration.getSourcesUpdatedAfter()));
		this._sourcesProvider.setUpdatedTo(configuration.getSourcesUpdatedBefore() == null ? null : new Date(configuration.getSourcesUpdatedBefore()));
		this._sourcesProvider.setSources(configuration.getSources());
		this._sourcesProvider.setProviderID("SOURCES");
		
		Class<? extends Matchlet<ExtendedVessel, ?, ExtendedVessel, ?>> matchletClass;
		
		try {
			for(MatchletConfiguration in : this._engineConfiguration.getMatchletsConfigurations()) {
				matchletClass = (Class<? extends Matchlet<ExtendedVessel, ?, ExtendedVessel, ?>>)Class.forName(in.getMatchletType());
				
				if(matchletClass.isAnnotationPresent(MatchletData.class))
					for(Class<?> attribute : matchletClass.getAnnotation(MatchletData.class).dataType())
						matchingAttributes.add((Class<?>)attribute);
				
				this._targetsProvider.setAttributes(matchingAttributes);
				this._sourcesProvider.setAttributes(matchingAttributes);
			}
		} catch(Throwable t) {
			throw new MatchingProcessException("Unable to retrieve matchlet metadata", t);
		}
		
		this._engine = new VesselsMatchingEngine();
		
		return this._engine.compareAll(this._engineConfiguration, 
									   this._handler, 
									   this._sourcesProvider, 
									   new IdentityDataPartitioner<ExtendedVessel, ExtendedVessel>(), 
									   this._targetsProvider,
									   new VesselIDHandler(),
									   new VesselIDHandler());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.driver.MatchingProcessDriver#haltProcess()
	 */
	@Override
	public void haltProcess() {
		this._handler.halt();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.driver.MatchingProcessDriver#dump()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> dump() throws Exception {
		try {
			MatchingsData<ExtendedVessel, ExtendedVessel> matchings = 
				JAXBHelper.fromXML(
					MatchingsData.class,
					new Class<?>[] { 
						ExtendedVessel.class,
						VesselsToFlags.class,
						VesselsToName.class,
						VesselsToIdentifiers.class,
						VesselsToCallsigns.class,
						VesselsToRegistrations.class,
						VesselsToLengths.class,
						VesselsToTonnage.class
					},
					JAXBHelper.toXML(
						new Class<?>[] { 
							ExtendedVessel.class,
							VesselsToFlags.class,
							VesselsToName.class,
							VesselsToIdentifiers.class,
							VesselsToCallsigns.class,
							VesselsToRegistrations.class,
							VesselsToLengths.class,
							VesselsToTonnage.class
						},
						this._handler.getCurrentResults()
					)
				);
					
				if(matchings != null)
					matchings = matchings.cleanup(this._engineConfiguration.getMaxCandidatesPerEntry(), 
												  this._engineConfiguration.getMinimumAllowedWeightedScore(), 
												  false);
				
			return 
				new MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration>(
					new MatchingEngineProcessorInfo<MatchingEngineProcessConfiguration>(this._engineConfiguration, this._handler.getProcessStatus()),
					matchings
				);
		} catch(Throwable t) {
			t.printStackTrace();
			
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.driver.MatchingProcessDriver#getStatus()
	 */
	@Override
	public MatchingEngineProcessStatus getProcessStatus() {
		return this._handler.getProcessStatus();
	}
}
