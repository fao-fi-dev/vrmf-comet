/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-vrmf-engine-gui-local)
 */
package org.fao.fi.comet.domain.vrmf.gui.local;

import org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver;
import org.fao.fi.comet.domain.vrmf.gui.VRMFMatchingEngineUI;

/**
 * @author Fiorellato
 */
public class VRMFLocalMatchingEngineUI extends VRMFMatchingEngineUI {
	public VRMFLocalMatchingEngineUI(MatchingEngineDriver processDriver) {
		super(processDriver);
	}

	static final public void main(String[] args) throws Throwable {
		VRMFMatchingEngineUI.launchUI();
	}
}
