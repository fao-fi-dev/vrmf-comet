/**
 * 
 */
package org.fao.fi.comet.domain.vrmf.gui.remote;

import org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver;
import org.fao.fi.comet.domain.vrmf.gui.VRMFMatchingEngineUI;

/**
 * @author Fiorellato
 *
 */
public class VRMFRemoteMatchingEngineUI extends VRMFMatchingEngineUI {
	public VRMFRemoteMatchingEngineUI(MatchingEngineDriver processDriver) {
		super(processDriver);
	}

	static final public void main(String[] args) throws Throwable {
		VRMFMatchingEngineUI.launchUI();
	}
}
