/**
 * 
 */
package org.fao.fi.comet.domain.vrmf.gui.remote.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CountVesselsResponse implements Serializable {
	private static final long serialVersionUID = -9036725671652940405L;
	
	@XmlElement(name="Vessels")
	private int _vessels;

	public CountVesselsResponse() {
		super();
	}

	public CountVesselsResponse(int vessels) {
		super();
		this._vessels = vessels;
	}

	public int getVessels() {
		return this._vessels;
	}

	public void setVessels(int vessels) {
		this._vessels = vessels;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this._vessels;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CountVesselsResponse other = (CountVesselsResponse) obj;
		if (this._vessels != other._vessels)
			return false;
		return true;
	}
}
