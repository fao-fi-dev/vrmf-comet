/**
 * 
 */
package org.fao.fi.comet.domain.vrmf.gui.remote.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CheckEngineStatusResponse implements Serializable {
	private static final long serialVersionUID = -9036725671652940405L;
	
	@XmlElement(name="IsRunning")
	private boolean _isRunning;

	public CheckEngineStatusResponse() {
		super();
	}

	public CheckEngineStatusResponse(boolean isRunning) {
		super();
		this._isRunning = isRunning;
	}

	public boolean getIsRunning() {
		return this._isRunning;
	}

	public void setIsRunning(boolean isRunning) {
		this._isRunning = isRunning;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this._isRunning ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckEngineStatusResponse other = (CheckEngineStatusResponse) obj;
		if (this._isRunning != other._isRunning)
			return false;
		return true;
	}
}
