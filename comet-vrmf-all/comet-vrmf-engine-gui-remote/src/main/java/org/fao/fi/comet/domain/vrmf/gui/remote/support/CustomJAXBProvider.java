/**
 * 
 */
package org.fao.fi.comet.domain.vrmf.gui.remote.support;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;

@Provider
public class CustomJAXBProvider implements ContextResolver<JAXBContext> {
	private JAXBContext _context = null;

	public JAXBContext getContext(Class<?> type) {
		if (type != MatchingEngineProcessResult.class) {
			try {
				return JAXBContext.newInstance(type);
			} catch(JAXBException Je) {
				throw new RuntimeException(Je);
			}
		}

		if (this._context == null) {
			try {
				this._context = JAXBContext.newInstance(
					type,
					VesselsToIdentifiers.class,
					VesselsToFlags.class,
					VesselsToName.class,
					VesselsToCallsigns.class,
					VesselsToRegistrations.class,
					VesselsToLengths.class,
					VesselsToTonnage.class,
					ExtendedVessel.class
				);
			} catch (JAXBException Je) {
				throw new RuntimeException(Je);
			}
		}

		return this._context;
	}
}