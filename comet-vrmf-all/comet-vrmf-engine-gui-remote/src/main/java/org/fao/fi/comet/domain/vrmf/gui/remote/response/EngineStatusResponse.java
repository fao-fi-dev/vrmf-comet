/**
 * 
 */
package org.fao.fi.comet.domain.vrmf.gui.remote.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EngineStatusResponse implements Serializable {
	private static final long serialVersionUID = 8824486518267809123L;
	
	@XmlElement(name="Status")
	private MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> _status;

	public EngineStatusResponse() {
		super();
	}

	public EngineStatusResponse(MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> status) {
		super();
		this._status = status;
	}

	public MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> getStatus() {
		return this._status;
	}

	public void setStatus(MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> status) {
		this._status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._status == null) ? 0 : this._status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EngineStatusResponse other = (EngineStatusResponse) obj;
		if (this._status == null) {
			if (other._status != null)
				return false;
		} else if (!this._status.equals(other._status))
			return false;
		return true;
	}
}	
