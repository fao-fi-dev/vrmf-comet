/**
 * 
 */
package org.fao.fi.comet.domain.vrmf.gui.remote.driver;

import java.util.concurrent.Future;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus;
import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;
import org.fao.fi.comet.domain.vrmf.common.spi.AbstractMatchingEngineDriver;
import org.fao.fi.comet.domain.vrmf.gui.remote.response.CountVesselsResponse;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.message.GZipEncoder;

/**
 * @author Fiorellato
 *
 */
@Singleton @Named("matching.engine.driver.remote")
public class RemoteMatchingEngineDriver extends AbstractMatchingEngineDriver {
	private Client _client;
	private String _endpoint = "http://localhost:8080/comet/vrmf/mapper/";
	
	final static protected String[] ACCEPTED_ENCODINGS = new String[] { "gzip", "deflate" };
	
	public RemoteMatchingEngineDriver() {
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(MultiPartFeature.class);
		clientConfig.register(new GZipEncoder());
		
		this._client = ClientBuilder.newClient(clientConfig);
		
		this._client.property(ClientProperties.CONNECT_TIMEOUT, 5000);
		this._client.property(ClientProperties.READ_TIMEOUT, 60 * 60 * 1000);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#describeSelf()
	 */
	@Override
	public String describeSelf() {
		return this._endpoint;
	}
	
	public String getEndpoint() {
		return this._endpoint;
	}

	public void setEndpoint(String endpoint) {
		this._endpoint = endpoint == null ? null : endpoint.endsWith("/") ? endpoint : ( endpoint + "/" );
	}

	protected WebTarget target(String service) {
		System.out.println("Targeting " + this._endpoint + service);
		return this._client.target(this._endpoint + service);
	}
	
	protected String asXML(MatchingProcessConfiguration configuration) {
		try {
			String xml = JAXBHelper.toXML(new Class[] { MatchingProcessConfiguration.class }, configuration);
			
			System.out.println("XML: " + xml);
			
			return xml;
		} catch(Throwable t) {
			throw new IllegalArgumentException("Unable to convert configuration to XML: " + t.getMessage(), t);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#countSources(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public int countSources(MatchingProcessConfiguration configuration) {
		Form form = new Form();
		form.param("configuration", configuration == null ? null : asXML(configuration));
		
		return this.target("count/sources").
						   request().
						   accept(MediaType.APPLICATION_XML).
						   acceptEncoding(ACCEPTED_ENCODINGS).
						   post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), CountVesselsResponse.class).getVessels();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#countTargets(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public int countTargets(MatchingProcessConfiguration configuration) {
		Form form = new Form();
		form.param("configuration", configuration == null ? null : asXML(configuration));
		
		return this.target("count/targets").
						   request().
						   accept(MediaType.APPLICATION_XML).
						   acceptEncoding(ACCEPTED_ENCODINGS).
						   post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), CountVesselsResponse.class).getVessels();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#getProcessStatus()
	 */
	@Override
	public MatchingEngineProcessStatus getProcessStatus() {
		return this.target("engine/status").
						   request().
						   accept(MediaType.APPLICATION_XML).
						   acceptEncoding(ACCEPTED_ENCODINGS).
						   get().readEntity(MatchingEngineProcessStatus.class);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#startProcess(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> startProcess(MatchingProcessConfiguration configuration) throws Exception {
		//System.out.println("Starting process on " + new Date() + " (" + System.currentTimeMillis() + ")");
		
		Form form = new Form();
		form.param("configuration", configuration == null ? null : asXML(configuration));
		
		try {
			Future<String> response = this.target("engine/match").
										   request().
										   accept(MediaType.APPLICATION_XML).
										   acceptEncoding(ACCEPTED_ENCODINGS).
										   async().
										   post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE),
											new InvocationCallback<String>() {
												@Override
												public void completed(String response) {
													//System.out.println("Response entity '" + response + "' received.");
												}
				
												@Override
												public void failed(Throwable throwable) {
													//System.out.println("Invocation failed.");
													throwable.printStackTrace();
												}
											});
			
			String xml = response.get();
			
			//System.out.println("Received response on " + new Date() + " (" + System.currentTimeMillis() + ")");
			
			MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> result = 
				JAXBHelper.fromXML(
					MatchingEngineProcessResult.class,
					new Class[] { 
						VesselsToIdentifiers.class,
						VesselsToFlags.class,
						VesselsToName.class,
						VesselsToCallsigns.class,
						VesselsToRegistrations.class,
						VesselsToLengths.class,
						VesselsToTonnage.class,
						ExtendedVessel.class
					},
					xml);
			
			//System.out.println("Completing process on " + new Date() + " (" + System.currentTimeMillis() + ")");

			return result;
		} catch(Exception e) {
			e.printStackTrace();
			
			throw e;
		} finally {
			//System.out.println("Exiting process on " + new Date() + " (" + System.currentTimeMillis() + ")");
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#haltProcess()
	 */
	@Override
	public void haltProcess() {
		this.target("engine/halt").
				    request().
				    delete();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#getProcessConfiguration()
	 */
	@Override
	public MatchingProcessConfiguration getProcessConfiguration() {
		return this.target("engine/configuration").
						   request().
						   accept(MediaType.APPLICATION_XML).
						   acceptEncoding(ACCEPTED_ENCODINGS).
						   get().readEntity(MatchingProcessConfiguration.class);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#dump()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> dump() throws Exception {
		String xml = this.target("engine/dump").
								 request().
								 accept(MediaType.APPLICATION_XML).
								 acceptEncoding(ACCEPTED_ENCODINGS).
								 get().readEntity(String.class);
		
		return JAXBHelper.fromXML(
				MatchingEngineProcessResult.class,
				new Class[] { 
					VesselsToIdentifiers.class,
					VesselsToFlags.class,
					VesselsToName.class,
					VesselsToCallsigns.class,
					VesselsToRegistrations.class,
					VesselsToLengths.class,
					VesselsToTonnage.class,
					ExtendedVessel.class
				},
				xml);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#getRefreshTime()
	 */
	@Override
	public int getRefreshTime() {
		return 1000;
	}
	
	final static public void main(String[] args) throws Throwable {
		RemoteMatchingEngineDriver d = new RemoteMatchingEngineDriver();
		
		System.out.println("Sources: " + d.countSources(null));
		System.out.println("Targets: " + d.countTargets(null));
		System.out.println("Status: " + d.getProcessStatus());
		System.out.println("Config: " + d.getProcessConfiguration());
		System.out.println("Dump: " + d.dump());
	}
}

