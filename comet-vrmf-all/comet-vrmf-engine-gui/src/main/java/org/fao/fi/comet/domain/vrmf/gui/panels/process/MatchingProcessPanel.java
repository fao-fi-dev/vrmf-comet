package org.fao.fi.comet.domain.vrmf.gui.panels.process;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus;
import org.fao.fi.comet.core.model.engine.MatchingEngineStatus;
import org.fao.fi.comet.domain.vrmf.gui.panels.AbstractInputPanel;

public class MatchingProcessPanel extends AbstractInputPanel {
	private static final long serialVersionUID = -6537536232850425691L;
	
	private JTextField _currentComparisons;
	private JTextField _skippedComparisons;
	private JTextField _authNoMatches;
	private JTextField _authMatches;
	private JTextField _matches;
	private JTextField _reducedMatches;
	private JTextField _atomicThroughput;
	private JTextField _throughput;
	private JProgressBar _targetStatus;
	private JLabel _maxTargets;
	private JLabel _minTargets;
	private JButton _startProcess;
	private JButton _dumpResults;
	private JButton _haltProcess;
	private JLabel _minSources;
	private JLabel _maxSources;
	private JProgressBar _sourcesStatus;
	private JLabel _label;
	private JLabel _elapsed;

	private long _started;
	
	public MatchingProcessPanel() {
		this.initialize();
	}
	
	public long getStarted() {
		return this._started;
	}

	public void setStarted(long started) {
		this._started = started;
	}

	public void setMinProcess(int value) {
		this._targetStatus.setMinimum(value);
		
		this.setInt(this._minTargets, value);
	}
	
	public int getMinProcess() {
		return this.asInt(this._minTargets);
	}
	
	public void setMaxProcess(int value) {
		this._targetStatus.setMaximum(value);
		
		this.setInt(this._maxTargets, value);
	}
	
	public int getMaxProcess() {
		return this.asInt(this._maxTargets);
	}
	
	public void setCurrentComparison(int value) {
		this.setInt(this._currentComparisons, value);
	}
	
	public int getCurrentComparisons() {
		return this.asInt(this._currentComparisons);
	}
	
	public void setSkippedComparison(int value) {
		this.setInt(this._skippedComparisons, value);
	}
	
	public int getSkippedComparisons() {
		return this.asInt(this._skippedComparisons);
	}
	
	public void setAuthNoMatches(int value) {
		this.setInt(this._authNoMatches, value);
	}
	
	public int getAuthMatches() {
		return this.asInt(this._authMatches);
	}
	
	public void setAuthMatches(int value) {
		this.setInt(this._authMatches, value);
	}
	
	public int getMatches() {
		return this.asInt(this._authMatches);
	}
	
	public void setMatches(int value) {
		this.setInt(this._matches, value);
	}
	
	public int getAuthNoMatches() {
		return this.asInt(this._matches);
	}
	
	public void setReducedMatches(int value) {
		this.setInt(this._reducedMatches, value);
	}
	
	public int getReducedMatches() {
		return this.asInt(this._reducedMatches);
	}
	
	public void setThroughput(double value) {
		this.setDouble(this._throughput, value, 4);
	}
	
	public double getThroughput() {
		return this.asDouble(this._throughput);
	}
	
	public void setAtomicThroughput(double value) {
		this.setDouble(this._atomicThroughput, value, 4);
	}
	
	public double getAtomicThroughput() {
		return this.asDouble(this._atomicThroughput);
	}
	
	@Override
	protected void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		this.setLayout(gridBagLayout);

		JPanel processPanel = new JPanel();
		processPanel.setBorder(new TitledBorder(null, "Process", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbcProcessPanel = new GridBagConstraints();
		gbcProcessPanel.fill = GridBagConstraints.HORIZONTAL;
		gbcProcessPanel.weightx = 100.0;
		gbcProcessPanel.anchor = GridBagConstraints.NORTH;
		gbcProcessPanel.insets = new Insets(5, 5, 5, 0);
		gbcProcessPanel.gridx = 0;
		gbcProcessPanel.gridy = 0;
		add(processPanel, gbcProcessPanel);
		GridBagLayout gbl__processPanel = new GridBagLayout();
		processPanel.setLayout(gbl__processPanel);

		JLabel lblNewLabel = new JLabel("Processed TARGET data:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(0, 5, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		processPanel.add(lblNewLabel, gbc_lblNewLabel);

		_targetStatus = new JProgressBar();
		GridBagConstraints gbc__processStatus = new GridBagConstraints();
		gbc__processStatus.fill = GridBagConstraints.HORIZONTAL;
		gbc__processStatus.weightx = 80.0;
		gbc__processStatus.insets = new Insets(0, 5, 5, 5);
		gbc__processStatus.gridx = 3;
		gbc__processStatus.gridy = 1;
		processPanel.add(_targetStatus, gbc__processStatus);

		_maxTargets = new JLabel("0");
		GridBagConstraints gbc__maxProcess = new GridBagConstraints();
		gbc__maxProcess.weightx = 10.0;
		gbc__maxProcess.anchor = GridBagConstraints.WEST;
		gbc__maxProcess.insets = new Insets(0, 5, 5, 0);
		gbc__maxProcess.gridx = 5;
		gbc__maxProcess.gridy = 1;
		processPanel.add(_maxTargets, gbc__maxProcess);

		_minTargets = new JLabel("0");
		GridBagConstraints gbc__minProcess = new GridBagConstraints();
		gbc__minProcess.anchor = GridBagConstraints.EAST;
		gbc__minProcess.insets = new Insets(0, 5, 5, 5);
		gbc__minProcess.gridx = 2;
		gbc__minProcess.gridy = 1;
		processPanel.add(_minTargets, gbc__minProcess);
		
		JLabel lblNewLabel_8 = new JLabel("Processed SOURCES data:");
		GridBagConstraints gbc_lblNewLabel_8 = new GridBagConstraints();
		gbc_lblNewLabel_8.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_8.insets = new Insets(0, 5, 5, 5);
		gbc_lblNewLabel_8.gridx = 0;
		gbc_lblNewLabel_8.gridy = 0;
		processPanel.add(lblNewLabel_8, gbc_lblNewLabel_8);
		
		_minSources = new JLabel("0");
		GridBagConstraints gbc__minSources = new GridBagConstraints();
		gbc__minSources.anchor = GridBagConstraints.EAST;
		gbc__minSources.insets = new Insets(0, 5, 5, 5);
		gbc__minSources.gridx = 2;
		gbc__minSources.gridy = 0;
		processPanel.add(_minSources, gbc__minSources);
		
		_sourcesStatus = new JProgressBar();
		GridBagConstraints gbc__sourcesStatus = new GridBagConstraints();
		gbc__sourcesStatus.fill = GridBagConstraints.HORIZONTAL;
		gbc__sourcesStatus.insets = new Insets(0, 5, 5, 5);
		gbc__sourcesStatus.gridx = 3;
		gbc__sourcesStatus.gridy = 0;
		processPanel.add(_sourcesStatus, gbc__sourcesStatus);
		
		_maxSources = new JLabel("0");
		GridBagConstraints gbc__maxSources = new GridBagConstraints();
		gbc__maxSources.anchor = GridBagConstraints.WEST;
		gbc__maxSources.insets = new Insets(0, 5, 5, 0);
		gbc__maxSources.gridx = 5;
		gbc__maxSources.gridy = 0;
		processPanel.add(_maxSources, gbc__maxSources);
		
		_label = new JLabel("Elapsed:");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.anchor = GridBagConstraints.WEST;
		gbc_label.insets = new Insets(0, 5, 5, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 2;
		processPanel.add(_label, gbc_label);
		
		_elapsed = new JLabel("00h:00m:00s");
		GridBagConstraints gbc_elapsed = new GridBagConstraints();
		gbc_elapsed.anchor = GridBagConstraints.WEST;
		gbc_elapsed.insets = new Insets(0, 5, 5, 5);
		gbc_elapsed.gridx = 3;
		gbc_elapsed.gridy = 2;
		processPanel.add(_elapsed, gbc_elapsed);

		JPanel comparisonsPanel = new JPanel();
		comparisonsPanel.setBorder(new TitledBorder(null, "Comparisons", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbcComparisonsPanel = new GridBagConstraints();
		gbcComparisonsPanel.anchor = GridBagConstraints.NORTH;
		gbcComparisonsPanel.insets = new Insets(5, 5, 5, 0);
		gbcComparisonsPanel.fill = GridBagConstraints.HORIZONTAL;
		gbcComparisonsPanel.gridx = 0;
		gbcComparisonsPanel.gridy = 1;
		add(comparisonsPanel, gbcComparisonsPanel);
		GridBagLayout gblComparisonsPanel = new GridBagLayout();
		gblComparisonsPanel.columnWidths = new int[] {150, 0, 0, 150, 0};
		comparisonsPanel.setLayout(gblComparisonsPanel);

		JLabel lblNewLabel_1 = new JLabel("Current comparisons:");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.weightx = 10.0;
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 5, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 0;
		comparisonsPanel.add(lblNewLabel_1, gbc_lblNewLabel_1);

		this._currentComparisons = new JTextField();
		this._currentComparisons.setHorizontalAlignment(SwingConstants.RIGHT);
		this._currentComparisons.setText("0");
		this._currentComparisons.setEditable(false);
		
		GridBagConstraints gbcCurrentComparisons = new GridBagConstraints();
		gbcCurrentComparisons.weightx = 20.0;
		gbcCurrentComparisons.insets = new Insets(0, 5, 5, 0);
		gbcCurrentComparisons.fill = GridBagConstraints.HORIZONTAL;
		gbcCurrentComparisons.gridx = 1;
		gbcCurrentComparisons.gridy = 0;
		comparisonsPanel.add(_currentComparisons, gbcCurrentComparisons);
		this._currentComparisons.setColumns(10);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbcHorizontalStrut_1 = new GridBagConstraints();
		gbcHorizontalStrut_1.fill = GridBagConstraints.HORIZONTAL;
		gbcHorizontalStrut_1.weightx = 10.0;
		gbcHorizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbcHorizontalStrut_1.gridx = 2;
		gbcHorizontalStrut_1.gridy = 0;
		comparisonsPanel.add(horizontalStrut_1, gbcHorizontalStrut_1);

		JLabel lblNewLabel_2 = new JLabel("Skipped comparisons:");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.weightx = 10.0;
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 5, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 1;
		comparisonsPanel.add(lblNewLabel_2, gbc_lblNewLabel_2);

		this._skippedComparisons = new JTextField();
		this._skippedComparisons.setHorizontalAlignment(SwingConstants.RIGHT);
		this._skippedComparisons.setText("0");
		this._skippedComparisons.setEditable(false);
		
		GridBagConstraints gbcSkippedComparisons = new GridBagConstraints();
		gbcSkippedComparisons.weightx = 20.0;
		gbcSkippedComparisons.insets = new Insets(0, 5, 5, 0);
		gbcSkippedComparisons.fill = GridBagConstraints.HORIZONTAL;
		gbcSkippedComparisons.gridx = 1;
		gbcSkippedComparisons.gridy = 1;
		comparisonsPanel.add(_skippedComparisons, gbcSkippedComparisons);
		this._skippedComparisons.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("Atomic throughput:");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_3.setToolTipText("Atomic comparisons / second");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.weightx = 10.0;
		gbc_lblNewLabel_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_3.insets = new Insets(0, 5, 5, 5);
		gbc_lblNewLabel_3.gridx = 3;
		gbc_lblNewLabel_3.gridy = 0;
		comparisonsPanel.add(lblNewLabel_3, gbc_lblNewLabel_3);

		this._atomicThroughput = new JTextField();
		this._atomicThroughput.setHorizontalAlignment(SwingConstants.RIGHT);
		this._atomicThroughput.setText("0");
		this._atomicThroughput.setEditable(false);
		
		GridBagConstraints gbcAtomicThroughput = new GridBagConstraints();
		gbcAtomicThroughput.weightx = 20.0;
		gbcAtomicThroughput.insets = new Insets(0, 5, 5, 0);
		gbcAtomicThroughput.fill = GridBagConstraints.HORIZONTAL;
		gbcAtomicThroughput.gridx = 4;
		gbcAtomicThroughput.gridy = 0;
		comparisonsPanel.add(_atomicThroughput, gbcAtomicThroughput);
		this._atomicThroughput.setColumns(10);

		JLabel lblNewLabel_4 = new JLabel("Throughput:");
		lblNewLabel_4.setToolTipText("Comparisons / second");
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.weightx = 10.0;
		gbc_lblNewLabel_4.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_4.insets = new Insets(0, 5, 5, 5);
		gbc_lblNewLabel_4.gridx = 3;
		gbc_lblNewLabel_4.gridy = 1;
		comparisonsPanel.add(lblNewLabel_4, gbc_lblNewLabel_4);

		this._throughput = new JTextField();
		this._throughput.setHorizontalAlignment(SwingConstants.RIGHT);
		this._throughput.setText("0");
		this._throughput.setEditable(false);
		
		GridBagConstraints gbcThroughput = new GridBagConstraints();
		gbcThroughput.weightx = 20.0;
		gbcThroughput.insets = new Insets(0, 5, 5, 0);
		gbcThroughput.fill = GridBagConstraints.HORIZONTAL;
		gbcThroughput.gridx = 4;
		gbcThroughput.gridy = 1;
		comparisonsPanel.add(_throughput, gbcThroughput);
		this._throughput.setColumns(10);

		JPanel matchingsPanel = new JPanel();
		matchingsPanel.setBorder(new TitledBorder(null, "Matchings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbcMatchingsPanel = new GridBagConstraints();
		gbcMatchingsPanel.insets = new Insets(5, 5, 5, 0);
		gbcMatchingsPanel.anchor = GridBagConstraints.NORTH;
		gbcMatchingsPanel.fill = GridBagConstraints.HORIZONTAL;
		gbcMatchingsPanel.gridx = 0;
		gbcMatchingsPanel.gridy = 2;
		add(matchingsPanel, gbcMatchingsPanel);
		
		GridBagLayout gbl__matchingsPanel = new GridBagLayout();
		gbl__matchingsPanel.columnWidths = new int[] {150, 0, 0, 150, 0};
		matchingsPanel.setLayout(gbl__matchingsPanel);

		JLabel lblNewLabel_5 = new JLabel("Authoritative NO MATCHes:");
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.weightx = 10.0;
		gbc_lblNewLabel_5.insets = new Insets(0, 5, 5, 5);
		gbc_lblNewLabel_5.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_5.gridx = 0;
		gbc_lblNewLabel_5.gridy = 0;
		matchingsPanel.add(lblNewLabel_5, gbc_lblNewLabel_5);

		this._authNoMatches = new JTextField();
		_authNoMatches.setEditable(false);
		this._authNoMatches.setHorizontalAlignment(SwingConstants.RIGHT);
		this._authNoMatches.setText("0");
		
		GridBagConstraints gbcAuthNoMatches = new GridBagConstraints();
		gbcAuthNoMatches.weightx = 20.0;
		gbcAuthNoMatches.insets = new Insets(0, 5, 5, 0);
		gbcAuthNoMatches.fill = GridBagConstraints.HORIZONTAL;
		gbcAuthNoMatches.gridx = 1;
		gbcAuthNoMatches.gridy = 0;
		matchingsPanel.add(_authNoMatches, gbcAuthNoMatches);
		this._authNoMatches.setColumns(10);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbcHorizontalStrut_2 = new GridBagConstraints();
		gbcHorizontalStrut_2.fill = GridBagConstraints.HORIZONTAL;
		gbcHorizontalStrut_2.weightx = 10.0;
		gbcHorizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbcHorizontalStrut_2.gridx = 2;
		gbcHorizontalStrut_2.gridy = 0;
		matchingsPanel.add(horizontalStrut_2, gbcHorizontalStrut_2);

		JLabel lblNewLabel_6 = new JLabel("Identified matches:");
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		
		gbc_lblNewLabel_6.weightx = 10.0;
		gbc_lblNewLabel_6.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_6.insets = new Insets(0, 5, 0, 5);
		gbc_lblNewLabel_6.gridx = 3;
		gbc_lblNewLabel_6.gridy = 0;
		matchingsPanel.add(lblNewLabel_6, gbc_lblNewLabel_6);

		this._matches = new JTextField();
		_matches.setEditable(false);
		this._matches.setHorizontalAlignment(SwingConstants.RIGHT);
		this._matches.setText("0");
		
		GridBagConstraints gbcMatches = new GridBagConstraints();
		gbcMatches.weightx = 20.0;
		gbcMatches.insets = new Insets(0, 5, 0, 0);
		gbcMatches.fill = GridBagConstraints.HORIZONTAL;
		gbcMatches.gridx = 4;
		gbcMatches.gridy = 0;
		matchingsPanel.add(_matches, gbcMatches);
		this._matches.setColumns(10);

		JLabel lblAuthoritativeMatches = new JLabel("Authoritative MATCHes:");
		GridBagConstraints gbc_lblAuthoritativeMatches = new GridBagConstraints();
		gbc_lblAuthoritativeMatches.weightx = 10.0;
		gbc_lblAuthoritativeMatches.anchor = GridBagConstraints.WEST;
		gbc_lblAuthoritativeMatches.insets = new Insets(0, 5, 5, 5);
		gbc_lblAuthoritativeMatches.gridx = 0;
		gbc_lblAuthoritativeMatches.gridy = 1;
		matchingsPanel.add(lblAuthoritativeMatches, gbc_lblAuthoritativeMatches);

		this._authMatches = new JTextField();
		_authMatches.setEditable(false);
		this._authMatches.setHorizontalAlignment(SwingConstants.RIGHT);
		this._authMatches.setText("0");
		
		GridBagConstraints gbcAuthMatches = new GridBagConstraints();
		gbcAuthMatches.weightx = 20.0;
		gbcAuthMatches.insets = new Insets(0, 5, 5, 0);
		gbcAuthMatches.fill = GridBagConstraints.HORIZONTAL;
		gbcAuthMatches.gridx = 1;
		gbcAuthMatches.gridy = 1;
		matchingsPanel.add(_authMatches, gbcAuthMatches);
		this._authMatches.setColumns(10);

		JLabel lblNewLabel_7 = new JLabel("Reduced matches:");
		GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
		gbc_lblNewLabel_7.weightx = 10.0;
		gbc_lblNewLabel_7.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_7.insets = new Insets(0, 5, 0, 5);
		gbc_lblNewLabel_7.gridx = 3;
		gbc_lblNewLabel_7.gridy = 1;
		matchingsPanel.add(lblNewLabel_7, gbc_lblNewLabel_7);

		this._reducedMatches = new JTextField();
		_reducedMatches.setEditable(false);
		this._reducedMatches.setHorizontalAlignment(SwingConstants.RIGHT);
		this._reducedMatches.setText("0");
		
		GridBagConstraints gbcReducedMatches = new GridBagConstraints();
		gbcReducedMatches.insets = new Insets(0, 5, 0, 0);
		gbcReducedMatches.weightx = 20.0;
		gbcReducedMatches.fill = GridBagConstraints.HORIZONTAL;
		gbcReducedMatches.gridx = 4;
		gbcReducedMatches.gridy = 1;
		matchingsPanel.add(_reducedMatches, gbcReducedMatches);
		this._reducedMatches.setColumns(10);

		Component verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 0);
		gbc_verticalStrut.fill = GridBagConstraints.VERTICAL;
		gbc_verticalStrut.weighty = 100.0;
		gbc_verticalStrut.gridx = 0;
		gbc_verticalStrut.gridy = 3;
		add(verticalStrut, gbc_verticalStrut);
		
		JPanel _commandPanel = new JPanel();
		GridBagConstraints gbc__commandPanel = new GridBagConstraints();
		gbc__commandPanel.fill = GridBagConstraints.BOTH;
		gbc__commandPanel.gridx = 0;
		gbc__commandPanel.gridy = 4;
		add(_commandPanel, gbc__commandPanel);
		
		_startProcess = new JButton("Start process");
		_commandPanel.add(_startProcess);
		
		_dumpResults = new JButton("Dump results");
		_dumpResults.setEnabled(false);
		_commandPanel.add(_dumpResults);
		
		_haltProcess = new JButton("Halt process");
		_haltProcess.setEnabled(false);
		_commandPanel.add(_haltProcess);
	}

	@Override
	public GridBagConstraints getConstraints() {
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(0, 0, 5, 0);
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridx = 0;
		constraints.gridy = 0;

		return constraints;
	}
	
	public void update(MatchingEngineProcessStatus status) {
		this.setInt(this._authMatches, status.getNumberOfAuthoritativeFullMatches());
		this.setInt(this._authNoMatches, status.getNumberOfAuthoritativeNoMatches());
		this.setInt(this._matches, status.getNumberOfMatches());
		
		this.setInt(this._minTargets, 0);
		this.setInt(this._maxTargets, status.getNumberOfComparisonRounds());
		
		this._targetStatus.setMaximum(status.getNumberOfComparisonRounds());
		this._sourcesStatus.setMaximum(status.getMaximumNumberOfAtomicComparisonsPerformedInRound());
		
		this.setInt(this._minSources, 0);
		this.setInt(this._maxSources, status.getMaximumNumberOfAtomicComparisonsPerformedInRound());

		long elapsed = 
			status.is(MatchingEngineStatus.RUNNING) ? 
				this._started <= 0 ? 
					status.getElapsed() : 
					System.currentTimeMillis() - this._started
				: status.getElapsed();
		
		int targetsCompared = status.getCurrentNumberOfComparisonRoundsPerformed();
		int sourcesCompared = status.getCurrentNumberOfAtomicComparisonsPerformedInRound();
		
		double throughput = 0;
		
		if(elapsed == 0)
			throughput = targetsCompared == 0 ? 0D : Double.POSITIVE_INFINITY;
		else
			throughput = targetsCompared * 1D / elapsed;
		
		this.setDouble(this._atomicThroughput, status.getAtomicThroughput(), 3);
		this.setDouble(this._throughput, throughput, 3);
		
		this._targetStatus.setValue(targetsCompared);
		this._sourcesStatus.setValue(sourcesCompared);
		
		this.setInt(this._currentComparisons, status.getTotalNumberOfAtomicComparisonsPerformed());
		this.setInt(this._skippedComparisons, status.getNumberOfAtomicComparisonsSkipped());
		
		long hours = elapsed / 1000 / 3600;
		long minutes = ( elapsed - ( hours * 1000 * 3600 ) ) / 1000 / 60 ;
		long seconds = ( elapsed - ( hours * 1000 * 3600 ) - ( minutes * 1000 * 60) ) / 1000;
		
		this._elapsed.setText(INTEGER_2_FORMATTER.format(hours) + "h:" + 
							  INTEGER_2_FORMATTER.format(minutes) + "m:" + 
							  INTEGER_2_FORMATTER.format(seconds) + "s");
	}
	
	public JButton getStartProcess() {
		return _startProcess;
	}
	
	public JButton getDumpResults() {
		return _dumpResults;
	}
	
	public JButton getHaltProcess() {
		return _haltProcess;
	}
	
	public void processIsRunning() {
		this._startProcess.setEnabled(false);
		this._haltProcess.setEnabled(true);
		this._dumpResults.setEnabled(true);
	}
	
	public void processIsIdle() {
		this._startProcess.setEnabled(true);
		this._haltProcess.setEnabled(false);
		this._dumpResults.setEnabled(false);
	}
	
	public void processHasCompleted() {
		this._startProcess.setEnabled(true);
		this._haltProcess.setEnabled(false);
		this._dumpResults.setEnabled(true);
	}
	
	public void processWasHalted() {
		this.processHasCompleted();
	}
	
	public JLabel getElapsed() {
		return _elapsed;
	}
}
