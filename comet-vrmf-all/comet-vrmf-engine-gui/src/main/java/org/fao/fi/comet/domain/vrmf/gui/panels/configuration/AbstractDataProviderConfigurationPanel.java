package org.fao.fi.comet.domain.vrmf.gui.panels.configuration;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import org.fao.fi.comet.domain.vrmf.gui.panels.AbstractInputPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel;
import org.jdesktop.swingx.JXDatePicker;

abstract public class AbstractDataProviderConfigurationPanel extends AbstractInputPanel implements ConfigurablePanel {
	private static final long serialVersionUID = -2982011824250544454L;

	static final private Date TODAY = new Date();
	static final private Date YESTERDAY = new Date(TODAY.getTime() - 86400 * 1000);

	protected JCheckBox _CCSBT;
	protected JCheckBox _IATTC;
	protected JCheckBox _ICCAT;
	protected JCheckBox _IOTC;
	protected JCheckBox _WCPFC;
	protected JXDatePicker _updatedAfter;
	protected JXDatePicker _updatedBefore;
	protected JToggleButton _enableUpdatedBefore;
	protected JToggleButton _enableUpdatedAfter;
	protected JButton _checkData;
	
	protected ActionListener _checkListener;

	public AbstractDataProviderConfigurationPanel() {
		super();
		
		this.initialize();
	}
	
	public AbstractDataProviderConfigurationPanel(ActionListener checkListener) {
		super();
		
		this._checkListener = checkListener;
		
		this.initialize();
	}

	@Override
	protected void initialize() {
		this.setLayout(new GridBagLayout());
		
		JLabel lblNewLabel_2 = new JLabel("Limit to vessels from:");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(5, 5, 5, 5);
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 0;
		this.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.gridheight = 2;
		gbc_horizontalStrut_1.weightx = 100.0;
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut_1.gridx = 5;
		gbc_horizontalStrut_1.gridy = 0;
		this.add(horizontalStrut_1, gbc_horizontalStrut_1);
		
		this._enableUpdatedAfter = new JToggleButton("Updated after:");
		GridBagConstraints gbc__srcUpdatedAfter = new GridBagConstraints();
		gbc__srcUpdatedAfter.insets = new Insets(0, 0, 0, 5);
		gbc__srcUpdatedAfter.gridx = 0;
		gbc__srcUpdatedAfter.gridy = 1;
		this.add(_enableUpdatedAfter, gbc__srcUpdatedAfter);
		
		JPanel sourcesSelection = new JPanel();
		sourcesSelection.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		GridBagConstraints gbc__sourcesSelection = new GridBagConstraints();
		gbc__sourcesSelection.gridwidth = 3;
		gbc__sourcesSelection.insets = new Insets(0, 0, 5, 5);
		gbc__sourcesSelection.fill = GridBagConstraints.BOTH;
		gbc__sourcesSelection.gridx = 1;
		gbc__sourcesSelection.gridy = 0;
		this.add(sourcesSelection, gbc__sourcesSelection);
		
		this._CCSBT = new JCheckBox("CCSBT");
		sourcesSelection.add(_CCSBT);
		this._CCSBT.setHorizontalAlignment(SwingConstants.LEFT);
		
		this._IATTC = new JCheckBox("IATTC");
		sourcesSelection.add(_IATTC);
		this._IATTC.setHorizontalAlignment(SwingConstants.LEFT);
		
		this._ICCAT = new JCheckBox("ICCAT");
		sourcesSelection.add(_ICCAT);
		this._ICCAT.setHorizontalAlignment(SwingConstants.LEFT);
		
		this._IOTC = new JCheckBox("IOTC");
		sourcesSelection.add(_IOTC);
		this._IOTC.setHorizontalAlignment(SwingConstants.LEFT);
		
		this._WCPFC = new JCheckBox("WCPFC");
		sourcesSelection.add(_WCPFC);
		this._WCPFC.setHorizontalAlignment(SwingConstants.LEFT);
		
		this._updatedAfter = new JXDatePicker();
		this._updatedAfter.setFormats(new String[] {"yyyy/MM/dd"});
		this._updatedAfter.setDate(YESTERDAY);
		
		GridBagConstraints gbc__sourcesUpdatedAfter = new GridBagConstraints();
		gbc__sourcesUpdatedAfter.insets = new Insets(0, 0, 0, 5);
		gbc__sourcesUpdatedAfter.gridx = 1;
		gbc__sourcesUpdatedAfter.gridy = 1;
		this.add(_updatedAfter, gbc__sourcesUpdatedAfter);
		
		this._enableUpdatedBefore = new JToggleButton("Updated before:");
		GridBagConstraints gbc__srcUpdatedBefore = new GridBagConstraints();
		gbc__srcUpdatedBefore.insets = new Insets(0, 0, 0, 5);
		gbc__srcUpdatedBefore.gridx = 2;
		gbc__srcUpdatedBefore.gridy = 1;
		this.add(_enableUpdatedBefore, gbc__srcUpdatedBefore);
		
		this._updatedBefore = new JXDatePicker();
		this._updatedBefore.setFormats(new String[] {"yyyy/MM/dd"});
		this._updatedBefore.setDate(TODAY);
		
		GridBagConstraints gbc__sourcesUpdatedBefore = new GridBagConstraints();
		gbc__sourcesUpdatedBefore.insets = new Insets(0, 0, 0, 5);
		gbc__sourcesUpdatedBefore.gridx = 3;
		gbc__sourcesUpdatedBefore.gridy = 1;
		this.add(_updatedBefore, gbc__sourcesUpdatedBefore);
		
		this._checkData = new JButton("Check");
		GridBagConstraints gbc__checkSources = new GridBagConstraints();
		gbc__checkSources.fill = GridBagConstraints.HORIZONTAL;
		gbc__checkSources.gridheight = 2;
		gbc__checkSources.insets = new Insets(0, 5, 0, 5);
		gbc__checkSources.gridx = 4;
		gbc__checkSources.gridy = 0;
		this.add(_checkData, gbc__checkSources);
		
		this.setCheckActionListener(this._checkListener);
		
		this._updatedAfter.setEnabled(this._enableUpdatedAfter.isSelected());
		this._updatedBefore.setEnabled(this._enableUpdatedBefore.isSelected());

		final JXDatePicker $updatedAfter = this._updatedAfter;
		final JToggleButton $enableUpdatedAfter = this._enableUpdatedAfter;
		
		final JXDatePicker $updatedBefore = this._updatedBefore;
		final JToggleButton $enableUpdatedBefore = this._enableUpdatedBefore;
		
		this._enableUpdatedAfter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				$updatedAfter.setEnabled($enableUpdatedAfter.isSelected());
			}
		});
		
		this._enableUpdatedBefore.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				$updatedBefore.setEnabled($enableUpdatedBefore.isSelected());
			}
		});
	}
	
	public void setCheckActionListener(ActionListener listener) {
		this._checkListener = listener;
		
		if(this._checkListener != null)
			this._checkData.addActionListener(this._checkListener);
	}
	
	@Override
	public GridBagConstraints getConstraints() {
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.weightx = 100.0;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.insets = new Insets(0, 0, 5, 0);
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridx = 0;
		constraints.gridy = 0;
		
		return constraints;
	}
}
