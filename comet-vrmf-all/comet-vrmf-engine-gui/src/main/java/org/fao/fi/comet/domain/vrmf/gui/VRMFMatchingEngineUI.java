package org.fao.fi.comet.domain.vrmf.gui;

import java.awt.Component;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;

import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingWorker;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.model.engine.MatchingEngineStatus;
import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;
import org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver;
import org.fao.fi.comet.domain.vrmf.gui.panels.configuration.AbstractDataProviderConfigurationPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.configuration.ProcessConfigurationPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.configuration.SourcesDataProviderConfigurationPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.configuration.TargetsDataProviderConfigurationPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.matchlets.DifferentUIDMatchletPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.matchlets.FlagMatchletPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.matchlets.IRCSMatchletPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.matchlets.LengthMatchletPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.matchlets.NameMatchletPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.matchlets.RegNoMatchletPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.matchlets.TonnageMatchletPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.matchlets.UniqueIdentifiersMatchletPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.process.MatchingProcessPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.process.RuntimePanel;
import org.fao.fi.comet.domain.vrmf.gui.support.UIUtils;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class VRMFMatchingEngineUI {
	static final public String MAPPINGS_FILE_TYPE = "mappings";
	static final public String MAPPINGS_FILE_EXTENSION = "." + MAPPINGS_FILE_TYPE;
	
	static final private FileNameExtensionFilter MAPPINGS_FILE_FILTER = new FileNameExtensionFilter("CLAV mappings", MAPPINGS_FILE_TYPE);
	
	private JFrame _frame;

	private Timer _updaterTimer;
	
	private ProcessConfigurationPanel _configurationPanel;
	private MatchingProcessPanel _processPanel;
	
	private AbstractDataProviderConfigurationPanel _sourcesConfiguration;
	private AbstractDataProviderConfigurationPanel _targetsConfiguration;

	private DifferentUIDMatchletPanel _differentUIDMatchletPanel;
	private UniqueIdentifiersMatchletPanel _uniqueIdentifiersMatchletPanel;
	private FlagMatchletPanel _flagMatchletPanel;
	private NameMatchletPanel _nameMatchletPanel;
	private IRCSMatchletPanel _ircsMatchletPanel;
	private RegNoMatchletPanel _regNoMatchletPanel;
	private LengthMatchletPanel _lengthMatchletPanel;
	private TonnageMatchletPanel _tonnageMatchletPanel;
	
	private RuntimePanel _runtimePanel;

	private MatchingEngineDriver _processDriver;
	
	private boolean _alreadyDumped = false;
	
	/**
	 * Create the application.
	 */
	public VRMFMatchingEngineUI(MatchingEngineDriver processDriver) {
		this._processDriver = processDriver;
		
		initialize();
	}
	
	private void setIdle() {
		this._runtimePanel.updateStatusText("Idle...");
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();

		this._frame = new JFrame();

		this._frame.setType(Type.POPUP);
		this._frame.setTitle("Matching Engine Controller [ " + this._processDriver.describeSelf() + " ]");
		this._frame.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		this._frame.setResizable(false);
		this._frame.setAutoRequestFocus(true);
		
		this._frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this._frame.setSize(new Dimension(600, 700));

		this._frame.setLocation(
			(int)((screen.width - this._frame.getSize().getWidth()) / 2), 
			(int)((screen.height - this._frame.getSize().getHeight()) / 2)
		);

		this._frame.setPreferredSize(this._frame.getSize());
		this._frame.setMinimumSize(this._frame.getSize());

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.rowHeights = new int[] {0, 24};
		this._frame.getContentPane().setLayout(gridBagLayout);
		
		JTabbedPane _mainTabs = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbcMainTabs = new GridBagConstraints();
		gbcMainTabs.anchor = GridBagConstraints.NORTH;
		gbcMainTabs.insets = new Insets(0, 0, 5, 0);
		gbcMainTabs.weighty = 100.0;
		gbcMainTabs.weightx = 100.0;
		gbcMainTabs.fill = GridBagConstraints.BOTH;
		gbcMainTabs.gridx = 0;
		gbcMainTabs.gridy = 0;
		
		this._frame.getContentPane().add(_mainTabs, gbcMainTabs);
		
		_mainTabs.addTab("Engine configuration", null, this.getConfigurationPanel(), null);
		_mainTabs.addTab("Matchlets configuration", null, this.getMatchletsPanel(), null);
		_mainTabs.addTab("Process", null, this.getMatchingProcessPanel(), null);
		
		JPanel runtimePanelContainer = new JPanel();
		runtimePanelContainer.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		GridBagConstraints gbcRuntimePanelContainer = new GridBagConstraints();
		gbcRuntimePanelContainer.insets = new Insets(0, 5, 5, 5);
		gbcRuntimePanelContainer.anchor = GridBagConstraints.WEST;
		gbcRuntimePanelContainer.fill = GridBagConstraints.HORIZONTAL;
		gbcRuntimePanelContainer.gridx = 0;
		gbcRuntimePanelContainer.gridy = 1;
		gbcRuntimePanelContainer.weightx = 100;
		
		this._frame.getContentPane().add(runtimePanelContainer, gbcRuntimePanelContainer);
		runtimePanelContainer.setLayout(new GridBagLayout());
		
		GridBagConstraints gbcRuntimePanel = new GridBagConstraints();
		gbcRuntimePanel.insets = new Insets(0, 0, 0, 0);
		gbcRuntimePanel.anchor = GridBagConstraints.WEST;
		gbcRuntimePanel.fill = GridBagConstraints.HORIZONTAL;
		gbcRuntimePanel.gridx = 0;
		gbcRuntimePanel.gridy = 0;
		gbcRuntimePanel.weightx = 100;
		
		this._runtimePanel = new RuntimePanel();
		
		runtimePanelContainer.add(this._runtimePanel, gbcRuntimePanel);
		
		final MatchingEngineDriver $driver = this._processDriver;
		final VRMFMatchingEngineUI $this = this;
		
		this._sourcesConfiguration.setCheckActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					int vessels = $driver.countSources($this.getCurrentConfiguration());
					
					if(vessels == 0)
						UIUtils.warning($this._frame.getContentPane(), "Identification results", "No source vessels can be identified according to the selected criteria");
					else
						UIUtils.info($this._frame.getContentPane(), "Identification results", vessels + " source vessels were identified according to the selected criteria");
				} catch(Throwable t) {
					t.printStackTrace();
					
					UIUtils.error($this._frame.getContentPane(), "Unable to count the number of selected source vessels", t.getMessage());
				} finally {
					$this.setIdle();
				}
			}
		});
		
		this._targetsConfiguration.setCheckActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					int vessels = $driver.countTargets($this.getCurrentConfiguration());
					
					if(vessels == 0)
						UIUtils.warning($this._frame.getContentPane(), "Identification results", "No target vessels can be identified according to the selected criteria");
					else
						UIUtils.info($this._frame.getContentPane(), "Identification results", vessels + " target vessels where identified according to the selected criteria");
				} catch(Throwable t) {
					t.printStackTrace();
					
					UIUtils.error($this._frame.getContentPane(), "Unable to count the number of selected targets vessels", t.getMessage());
				} finally {
					$this.setIdle();
				}
			}
		});

		this._processPanel.getStartProcess().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingWorker<Void, Object> executor = new SwingWorker<Void, Object>() {
					@Override
					protected Void doInBackground() throws Exception {
						$this._alreadyDumped = false;
						
						$this._runtimePanel.updateStatusText("The matching process is running...");
						
						UIUtils.disableInputs((JComponent)$this._frame.getContentPane());

						$this._processPanel.processIsRunning();

						try {
							MatchingProcessConfiguration configuration = $this.getCurrentConfiguration();
							
							if(configuration.getMatchletConfigurations() == null || configuration.getMatchletConfigurations().isEmpty()) {
								UIUtils.error($this._frame.getContentPane(), "Unable to start the matching process", "No matchlet is currently enabled: the process cannot start");
							
								UIUtils.enableInputs((JComponent)$this._frame.getContentPane());
							} else {
								if(configuration.getMatchletConfigurations().size() == 1) {
									UIUtils.error($this._frame.getContentPane(), "Unable to start the matching process", "There's only one matchlet enabled and it is the 'Different TUVIs' matchlet: the process cannot start");
								} else {
									$this._processPanel.setStarted(System.currentTimeMillis());
									
									$this._processDriver.startProcess(configuration);
									
									$this._processPanel.processIsIdle();

									UIUtils.info($this._frame.getContentPane(), "Process completed!", "The matching process completed in " + $this._processPanel.getElapsed().getText());
								}
							}
						} catch(Throwable t) {
							t.printStackTrace();
							
							UIUtils.error($this._frame.getContentPane(), "Unexpected error caught while attempting to start the matching process", t.getMessage());
						}
						
						return null;
					}

					@Override
					protected void done() {
						UIUtils.enableInputs((JComponent)$this._frame.getContentPane());
						
						$this._processPanel.processIsIdle();
						$this.setIdle();
					}
				};
				
				executor.execute();
			}
		});
		
		this._processPanel.getHaltProcess().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						int confirm = JOptionPane.showConfirmDialog($this._frame.getContentPane(), "Are you sure you want to halt the currently running matching process?", "Please confirm", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

						try {
							if(confirm == 0) {
								$this._runtimePanel.updateStatusText("Halting the matching process...");
								
								$this._processDriver.haltProcess();
								
								UIUtils.warning($this._frame.getContentPane(), "Process halted!", "The matching process was halted after executing for " + $this._processPanel.getElapsed().getText() + "...");
							}
						} catch(Throwable t) {
							t.printStackTrace();
							
							UIUtils.error($this._frame.getContentPane(), "Unexpected error caught while attempting to halt the matching process", t.getMessage());
						} finally {
							if(confirm == 0) {
								UIUtils.enableInputs((JComponent)$this._frame.getContentPane());
								
								$this._processPanel.processIsIdle();
								
								$this.setIdle();
							}
						}
					}
				});
			}
		});
		
		this._processPanel.getDumpResults().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingWorker<Void, Object> executor = new SwingWorker<Void, Object>() {
					@Override
					protected Void doInBackground() throws Exception {
						try {
							$this._runtimePanel.updateStatusText("Dumping current matching data...");
							
							$this.dump($this._processDriver);
						} catch(Throwable t) {
							t.printStackTrace();
							
							UIUtils.error($this._frame.getContentPane(), "Unexpected error caught while attempting to dump the current matching process", t.getMessage());
						} 
						
						return null;
					}

					@Override
					protected void done() {
						if($this._processDriver.is(MatchingEngineStatus.RUNNING)) {
							$this._runtimePanel.updateStatusText("The matching process is running...");
						} else if($this._processDriver.is(MatchingEngineStatus.COMPLETED)) {
							$this._runtimePanel.updateStatusText("The matching process has completed...");
						} else if($this._processDriver.is(MatchingEngineStatus.HALTED)) {
							$this._runtimePanel.updateStatusText("The matching process was halted...");
						} else {
							$this.setIdle();
						}
					}
				};
				
				executor.execute();
			}
		});
		
		ActionListener processUpdater = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MatchingEngineStatus current = $this._processDriver.getProcessStatus().getStatus();
				
				boolean isRunning = MatchingEngineStatus.RUNNING.equals(current);
				boolean hasCompleted = MatchingEngineStatus.COMPLETED.equals(current);
				boolean wasHalted = MatchingEngineStatus.HALTED.equals(current);
				
				if(isRunning) {
					$this._runtimePanel.updateStatusText("The matching process is running...");

					UIUtils.disableInputs((JComponent)$this._frame.getContentPane());
					
					$this._processPanel.processIsRunning();
				} else {
					try {
						if(hasCompleted) {
							$this._processPanel.processHasCompleted();
						
							if(!$this._alreadyDumped) {
								$this._alreadyDumped = true;

								int a = JOptionPane.showConfirmDialog($this._frame, "Matching process results are available: do you want to store the mappings on your computer？", "Please confirm",
										JOptionPane.YES_NO_OPTION);
							
								if (a == 0) {
									$this.dump($this._processDriver);
								}
							}
						}
					} catch(Throwable t) {
						throw new RuntimeException("Unable to dump current process data", t);
					}
				}

				$this._processPanel.update($this._processDriver.getProcessStatus());
				
				if(!isRunning) {
					UIUtils.enableInputs((JComponent)$this._frame.getContentPane());
					
					if(hasCompleted) {
						$this._runtimePanel.updateStatusText("The matching process has completed...");
						
						$this._processPanel.processHasCompleted();
					} else if(wasHalted) {
						$this._runtimePanel.updateStatusText("The matching process was halted...");
						
						$this._processPanel.processWasHalted();
					} else
						$this._processPanel.processIsIdle();
				} else {
					$this._processPanel.processIsRunning();
				}
			}
		};
		
		ActionListener runtimeUpdater = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				$this._runtimePanel.updateStatus();
			}
		};
		
		this._frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				int a = JOptionPane.showConfirmDialog($this._frame, "Are you sure you want to close the application？", "Please confirm",
						JOptionPane.YES_NO_OPTION);
				if (a == 0) {
					$this._frame.dispose();
					
					System.exit(0);
				}
			}
		});
		
		this._updaterTimer = new Timer($driver.getRefreshTime(), processUpdater);
		this._updaterTimer.start();
		
		new Timer(100, runtimeUpdater).start();
	}

	private void dump(MatchingEngineDriver driver) throws Exception {
		this.dump(driver.dump());
	}
	
	private void dump(MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> data) throws Exception {
		JFileChooser chooser = new JFileChooser(new File("./mappings"));
		chooser.setDialogTitle("Choose a file to save current mappings into");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.addChoosableFileFilter(MAPPINGS_FILE_FILTER);
		chooser.setAcceptAllFileFilterUsed(false);
		chooser.setFileFilter(MAPPINGS_FILE_FILTER);
		
		int returnValue = chooser.showSaveDialog(this._frame.getContentPane());
		
		if(returnValue == JFileChooser.APPROVE_OPTION) {
			this._runtimePanel.updateStatusText("Saving the current mappings...");
			
			File selected = chooser.getSelectedFile();
			
			if(!selected.getName().endsWith(MAPPINGS_FILE_EXTENSION))
				selected = new File(selected.getParentFile(), selected.getName() + MAPPINGS_FILE_EXTENSION);
			
			FileOutputStream fos = null;
			
			try {
				fos = new FileOutputStream(selected);

				byte[] stream = 
					JAXBHelper.toXML(
						new Class<?>[] { 
							ExtendedVessel.class,
							VesselsToFlags.class,
							VesselsToName.class,
							VesselsToIdentifiers.class,
							VesselsToCallsigns.class,
							VesselsToRegistrations.class,
							VesselsToLengths.class,
							VesselsToTonnage.class
						}, 
						data
					).getBytes("UTF-8");
	
				fos.write(stream);
				fos.flush();
				
				UIUtils.info(this._frame.getContentPane(), "The mappings have been saved", "The current identified mappings have been saved in " + selected.getAbsolutePath() + " (" + stream.length + " bytes)");
			} catch(Throwable t) {
				UIUtils.error(this._frame.getContentPane(), "Unable to save mappings", t.getMessage());
			} finally {
				if(fos != null) {
					try {
						fos.close();
					} catch(Throwable t) {
						UIUtils.error(this._frame.getContentPane(), "Unable to close output stream for mapping file", t.getMessage());
					}
				}
			}
			
			this.setIdle();
		}
	}

	public MatchingProcessConfiguration getCurrentConfiguration() {
		MatchingProcessConfiguration configuration = new MatchingProcessConfiguration();
		this._configurationPanel.updateConfiguration(configuration);
		this._sourcesConfiguration.updateConfiguration(configuration);
		this._targetsConfiguration.updateConfiguration(configuration);

		this._differentUIDMatchletPanel.updateConfiguration(configuration);
		this._uniqueIdentifiersMatchletPanel.updateConfiguration(configuration);
		this._flagMatchletPanel.updateConfiguration(configuration);
		this._nameMatchletPanel.updateConfiguration(configuration);
		this._ircsMatchletPanel.updateConfiguration(configuration);
		this._regNoMatchletPanel.updateConfiguration(configuration);
		this._lengthMatchletPanel.updateConfiguration(configuration);
		this._tonnageMatchletPanel.updateConfiguration(configuration);
		
		return configuration;
	}
	
	public void updateFromConfiguration(MatchingProcessConfiguration configuration) {
		this._configurationPanel.initializeFromConfiguration(configuration);
		this._sourcesConfiguration.initializeFromConfiguration(configuration);
		this._targetsConfiguration.initializeFromConfiguration(configuration);
		
		this._differentUIDMatchletPanel.initializeFromConfiguration(configuration);
		this._uniqueIdentifiersMatchletPanel.initializeFromConfiguration(configuration);
		this._flagMatchletPanel.initializeFromConfiguration(configuration);
		this._nameMatchletPanel.initializeFromConfiguration(configuration);
		this._ircsMatchletPanel.initializeFromConfiguration(configuration);
		this._regNoMatchletPanel.initializeFromConfiguration(configuration);
		this._lengthMatchletPanel.initializeFromConfiguration(configuration);
		this._tonnageMatchletPanel.initializeFromConfiguration(configuration);
	}
	
	static final protected void launchUI() throws Throwable {
		final ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(new String[] {
			"config/comet/vrmf/engine/spring/spring-context.xml"
		});
		
		final MatchingEngineDriver matchingProcessDriver = ctx.getBean(MatchingEngineDriver.class);
		
		try {
			JFrame.setDefaultLookAndFeelDecorated(true);
			JDialog.setDefaultLookAndFeelDecorated(true);

			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable t) {
			ctx.close();
			
			throw new RuntimeException("Unable to create GUI", t);
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VRMFMatchingEngineUI window = new VRMFMatchingEngineUI(matchingProcessDriver);
					
					MatchingProcessConfiguration configuration = matchingProcessDriver.getProcessConfiguration();
					
					window._configurationPanel.updateConfiguration(configuration);
					window._sourcesConfiguration.updateConfiguration(configuration);
					window._targetsConfiguration.updateConfiguration(configuration);
					
					window._differentUIDMatchletPanel.updateConfiguration(configuration);
					window._uniqueIdentifiersMatchletPanel.updateConfiguration(configuration);
					window._flagMatchletPanel.updateConfiguration(configuration);
					window._nameMatchletPanel.updateConfiguration(configuration);
					window._ircsMatchletPanel.updateConfiguration(configuration);
					window._regNoMatchletPanel.updateConfiguration(configuration);
					window._lengthMatchletPanel.updateConfiguration(configuration);
					window._tonnageMatchletPanel.updateConfiguration(configuration);
					
					GridBagConstraints gbcProcessPanel = new GridBagConstraints();
					gbcProcessPanel.insets = new Insets(0, 0, 5, 0);
					gbcProcessPanel.fill = GridBagConstraints.BOTH;
					gbcProcessPanel.gridx = 0;
					gbcProcessPanel.gridy = 0;
					
					window._frame.setVisible(true);
					window._frame.toFront();
					
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
//					ctx.close();
				}
			}
		});
	}
	
	protected JPanel getConfigurationPanel() {
		JPanel configurationPanelContainer = new JPanel();
		configurationPanelContainer.setLayout(new GridBagLayout());

		this._sourcesConfiguration = new SourcesDataProviderConfigurationPanel();
		this._sourcesConfiguration.setBorder(new TitledBorder(null, "Sources filtering:", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		GridBagConstraints gbcSources = this._sourcesConfiguration.getConstraints();

		this._targetsConfiguration = new TargetsDataProviderConfigurationPanel();
		this._targetsConfiguration.setBorder(new TitledBorder(null, "Targets filtering:", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		GridBagConstraints gbcTargets = this._targetsConfiguration.getConstraints();
				
		GridBagConstraints gbc_sourcesConfiguration = new GridBagConstraints();
		gbc_sourcesConfiguration.fill = GridBagConstraints.HORIZONTAL;
		gbc_sourcesConfiguration.anchor = GridBagConstraints.NORTH;
		gbc_sourcesConfiguration.insets = new Insets(0, 0, 5, 5);
		gbc_sourcesConfiguration.gridx = 0;
		gbc_sourcesConfiguration.gridy = 0;
		configurationPanelContainer.add(this._sourcesConfiguration, gbc_sourcesConfiguration);

		GridBagConstraints gbc_targetsConfiguration = new GridBagConstraints();
		gbc_targetsConfiguration.weightx = 100.0;
		gbc_targetsConfiguration.fill = GridBagConstraints.HORIZONTAL;
		gbc_targetsConfiguration.anchor = GridBagConstraints.NORTH;
		gbc_targetsConfiguration.insets = new Insets(0, 0, 5, 5);
		gbc_targetsConfiguration.gridx = 0;
		gbc_targetsConfiguration.gridy = 1;
		configurationPanelContainer.add(this._targetsConfiguration, gbc_targetsConfiguration);

		Component verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.weighty = 100.0;
		gbc_verticalStrut.anchor = GridBagConstraints.NORTH;
		gbc_verticalStrut.fill = GridBagConstraints.HORIZONTAL;
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 0);
		gbc_verticalStrut.gridx = 0;
		gbc_verticalStrut.gridy = 3;
		configurationPanelContainer.add(verticalStrut, gbc_verticalStrut);

		this._configurationPanel = new ProcessConfigurationPanel();
		this._configurationPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Process parameters:", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_configurationPanel = new GridBagConstraints();
		gbc_configurationPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_configurationPanel.anchor = GridBagConstraints.NORTH;
		gbc_configurationPanel.insets = new Insets(0, 0, 0, 5);
		gbc_configurationPanel.gridx = 0;
		gbc_configurationPanel.gridy = 2;
		configurationPanelContainer.add(this._configurationPanel, gbc_configurationPanel);
		gbcSources.gridy = 0;
		gbcTargets.gridy = 1;

		return configurationPanelContainer;
	}
	
	protected JPanel getMatchletsPanel() {
		JPanel matchletsPanel = new JPanel();
		matchletsPanel.setLayout(new GridBagLayout());
		JTabbedPane matchletsTabs = new JTabbedPane(JTabbedPane.TOP);

		GridBagConstraints gbcMatchletsTabs = new GridBagConstraints();
		gbcMatchletsTabs.weighty = 100.0;
		gbcMatchletsTabs.weightx = 100.0;
		gbcMatchletsTabs.fill = GridBagConstraints.BOTH;
		gbcMatchletsTabs.anchor = GridBagConstraints.WEST;
		gbcMatchletsTabs.gridx = 0;
		gbcMatchletsTabs.gridy = 0;
		matchletsPanel.add(matchletsTabs, gbcMatchletsTabs);

		JPanel identifiersMatchletContainer = new JPanel();
		
		GridBagConstraints gbcIdentifiersTabs = new GridBagConstraints();
		gbcIdentifiersTabs.weighty = 100.0;
		gbcIdentifiersTabs.weightx = 100.0;
		gbcIdentifiersTabs.fill = GridBagConstraints.BOTH;
		gbcIdentifiersTabs.anchor = GridBagConstraints.WEST;
		gbcIdentifiersTabs.gridx = 0;
		gbcIdentifiersTabs.gridy = 0;
		
		identifiersMatchletContainer.setLayout(new GridBagLayout());
		
		JPanel flagMatcherContainer = new JPanel();
		flagMatcherContainer.setLayout(new GridBagLayout());
		flagMatcherContainer.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Vessel Flag Matchlet Configuration", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		JPanel nameMatcherContainer = new JPanel();
		nameMatcherContainer.setLayout(new GridBagLayout());
		nameMatcherContainer.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Vessel Name Matchlet Configuration", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		JPanel ircsMatcherContainer = new JPanel();
		ircsMatcherContainer.setLayout(new GridBagLayout());
		ircsMatcherContainer.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Vessel Radio Callsign Matchlet Configuration", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		JPanel regNoMatcherContainer = new JPanel();
		regNoMatcherContainer.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Vessel Registration Number Matchlet Configuration", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		regNoMatcherContainer.setLayout(new GridBagLayout());

		JPanel lengthMatcherContainer = new JPanel();
		lengthMatcherContainer.setLayout(new GridBagLayout());
		lengthMatcherContainer.setBorder(new TitledBorder(null, "Vessel Physical Dimensions Matchlet Configuration", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		JPanel tonnageMatcherContainer = new JPanel();
		tonnageMatcherContainer.setLayout(new GridBagLayout());
		tonnageMatcherContainer.setBorder(new TitledBorder(null, "Vessel Tonnages Matchlet Configuration", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		GridBagConstraints gbcDifferentUID = new GridBagConstraints();
		gbcDifferentUID.weighty = 100.0;
		gbcDifferentUID.weightx = 100.0;
		gbcDifferentUID.fill = GridBagConstraints.BOTH;
		gbcDifferentUID.anchor = GridBagConstraints.WEST;
		gbcDifferentUID.gridx = 0;
		gbcDifferentUID.gridy = 0;
		
		this._differentUIDMatchletPanel = new DifferentUIDMatchletPanel();
		
		identifiersMatchletContainer.add(this._differentUIDMatchletPanel, gbcDifferentUID); 
		
		GridBagConstraints gbcUniqueIdentifiers = new GridBagConstraints();
		gbcUniqueIdentifiers.weighty = 100.0;
		gbcUniqueIdentifiers.weightx = 100.0;
		gbcUniqueIdentifiers.fill = GridBagConstraints.BOTH;
		gbcUniqueIdentifiers.anchor = GridBagConstraints.WEST;
		gbcUniqueIdentifiers.gridx = 0;
		gbcUniqueIdentifiers.gridy = 1;
		
		this._uniqueIdentifiersMatchletPanel = new UniqueIdentifiersMatchletPanel();
		
		identifiersMatchletContainer.add(this._uniqueIdentifiersMatchletPanel, gbcUniqueIdentifiers);
		
		this._flagMatchletPanel = new FlagMatchletPanel();
		flagMatcherContainer.add(this._flagMatchletPanel, this._flagMatchletPanel.getConstraints());

		this._nameMatchletPanel = new NameMatchletPanel();
		nameMatcherContainer.add(this._nameMatchletPanel, this._nameMatchletPanel.getConstraints());

		this._ircsMatchletPanel = new IRCSMatchletPanel();
		ircsMatcherContainer.add(this._ircsMatchletPanel, this._ircsMatchletPanel.getConstraints());

		this._regNoMatchletPanel = new RegNoMatchletPanel();
		regNoMatcherContainer.add(this._regNoMatchletPanel, this._regNoMatchletPanel.getConstraints());

		this._lengthMatchletPanel = new LengthMatchletPanel();
		lengthMatcherContainer.add(this._lengthMatchletPanel, this._lengthMatchletPanel.getConstraints());

		this._tonnageMatchletPanel = new TonnageMatchletPanel();
		tonnageMatcherContainer.add(this._tonnageMatchletPanel, this._tonnageMatchletPanel.getConstraints());

		matchletsTabs.addTab("Vessel Identifiers", null, identifiersMatchletContainer, null);
		matchletsTabs.addTab("Vessel Flag", null, flagMatcherContainer, null);
		matchletsTabs.addTab("Vessel Name", null, nameMatcherContainer, null);
		matchletsTabs.addTab("Radio Callsign", null, ircsMatcherContainer, null);
		matchletsTabs.addTab("Registration Number", null, regNoMatcherContainer, null);
		matchletsTabs.addTab("Physical Dimensions", null, lengthMatcherContainer, null);
		matchletsTabs.addTab("Tonnages", null, tonnageMatcherContainer, null);

		return matchletsPanel;
	}
	
	private MatchingProcessPanel getMatchingProcessPanel() {
		this._processPanel = new MatchingProcessPanel();
		
		return this._processPanel;
	}
}
