package org.fao.fi.comet.domain.vrmf.gui.panels.matchlets;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JSlider;

import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.model.matchlets.MatchletConfigurationParameter;
import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;

public class NameMatchletPanel extends AbstractMatchletPanel {
	private static final long serialVersionUID = -4117474383272210139L;

	private JSlider _simplifiedNameWeight;
	private JSlider _soundexWeight;

	public NameMatchletPanel() {
		this.initialize();
	}
	
	@Override
	protected void initialize() {
		super.initialize();
		
		this._matcherWeight.setValue(40);
		this._matcherScore.setValue(75);

		JLabel label_7 = new JLabel("Simplified name weight:");
		label_7.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_label_7 = new GridBagConstraints();
		gbc_label_7.anchor = GridBagConstraints.WEST;
		gbc_label_7.insets = new Insets(0, 0, 5, 0);
		gbc_label_7.gridx = 0;
		gbc_label_7.gridy = 5;
		this.add(label_7, gbc_label_7);
		
		this._simplifiedNameWeight = new JSlider();
		this._simplifiedNameWeight.setValue(100);
		this._simplifiedNameWeight.setPaintTicks(true);
		this._simplifiedNameWeight.setPaintLabels(true);
		this._simplifiedNameWeight.setMinorTickSpacing(5);
		this._simplifiedNameWeight.setMajorTickSpacing(10);
		GridBagConstraints gbc__simplifiedNameWeight = new GridBagConstraints();
		gbc__simplifiedNameWeight.insets = new Insets(0, 0, 5, 0);
		gbc__simplifiedNameWeight.gridx = 0;
		gbc__simplifiedNameWeight.gridy = 6;
		this.add(this._simplifiedNameWeight, gbc__simplifiedNameWeight);
		
		JLabel label_8 = new JLabel("Soundex weight:");
		label_8.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_label_8 = new GridBagConstraints();
		gbc_label_8.anchor = GridBagConstraints.WEST;
		gbc_label_8.insets = new Insets(0, 0, 5, 0);
		gbc_label_8.gridx = 0;
		gbc_label_8.gridy = 7;
		this.add(label_8, gbc_label_8);
		
		this._soundexWeight = new JSlider();
		this._soundexWeight.setValue(25);
		this._soundexWeight.setPaintTicks(true);
		this._soundexWeight.setPaintLabels(true);
		this._soundexWeight.setMinorTickSpacing(5);
		this._soundexWeight.setMajorTickSpacing(10);
		GridBagConstraints gbc__soundexWeight = new GridBagConstraints();
		gbc__soundexWeight.insets = new Insets(0, 0, 5, 0);
		gbc__soundexWeight.gridx = 0;
		gbc__soundexWeight.gridy = 8;
		this.add(this._soundexWeight, gbc__soundexWeight);

		this.addMissingComponents(10);
	}
	
	@Override
	protected boolean isEnabledByDefault() {
		return true;
	}
	
	@Override
	protected boolean isOptionalByDefault() {
		return false;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#initializeFromConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void initializeFromConfiguration(MatchingProcessConfiguration configuration) {
		MatchletConfiguration current = this.findConfigurationFor("SmartVesselNameHistoricalMatchlet", configuration);
		
		super.commonInitializeFromConfiguration(current);
		
		if(current != null) {
			MatchletConfigurationParameter countryWeight = this.findConfigurationParameterFor("simplifiedNameWeight", current);
			if(countryWeight.getActualValue() != null) {
				this._simplifiedNameWeight.setValue((int)(Math.round(Double.parseDouble(countryWeight.getValue()))));
			}
			
			MatchletConfigurationParameter ircsWeight = this.findConfigurationParameterFor("soundexWeight", current);
			if(ircsWeight.getActualValue() != null) {
				this._soundexWeight.setValue((int)(Math.round(Double.parseDouble(ircsWeight.getValue()))));
			}
		}	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#updateConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void updateConfiguration(MatchingProcessConfiguration processConfiguration) {
		MatchletConfiguration currentConfiguration = super.commonUpdateConfiguration("SmartVesselNameHistoricalMatchlet", processConfiguration);
		
		if(currentConfiguration != null) {
			currentConfiguration.with("simplifiedNameWeight", this._simplifiedNameWeight.getValue());
			currentConfiguration.with("soundexWeight", this._soundexWeight.getValue());
		}	
	}
}
