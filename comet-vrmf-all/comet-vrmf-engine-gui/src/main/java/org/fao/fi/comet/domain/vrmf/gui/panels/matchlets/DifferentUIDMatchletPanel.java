package org.fao.fi.comet.domain.vrmf.gui.panels.matchlets;

import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;


public class DifferentUIDMatchletPanel extends AbstractMatchletPanel {
	private static final long serialVersionUID = -4117474383272210139L;

	public DifferentUIDMatchletPanel() {
		this.initialize();
	}
	
	@Override
	protected void initialize() {
		super.initialize();
	
		this._matcherWeight.setValue(1);
		this._matcherScore.setValue(100);
		
		this.addMissingComponents(5);

		this._matcherWeight.setVisible(false);
		this._matcherWeightLabel.setVisible(false);
		this._matcherScore.setVisible(false);
		this._matcherScoreLabel.setVisible(false);
		this._matcherOptional.setVisible(false);

		this.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Different TUVIs Matchlet Configuration", TitledBorder.LEADING, TitledBorder.TOP, null, null));
	}
	
	@Override
	protected boolean isEnabledByDefault() {
		return true;
	}
	
	@Override
	protected boolean isOptionalByDefault() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#initializeFromConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void initializeFromConfiguration(MatchingProcessConfiguration configuration) {
		super.commonInitializeFromConfiguration(this.findConfigurationFor("DifferentUIDMatchlet", configuration));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#updateConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void updateConfiguration(MatchingProcessConfiguration configuration) {
		super.commonUpdateConfiguration("DifferentUIDMatchlet", configuration);
	}
}
