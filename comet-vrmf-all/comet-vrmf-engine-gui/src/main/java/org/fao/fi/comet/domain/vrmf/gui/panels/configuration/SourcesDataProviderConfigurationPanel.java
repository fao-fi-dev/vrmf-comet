package org.fao.fi.comet.domain.vrmf.gui.panels.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;

public class SourcesDataProviderConfigurationPanel extends AbstractDataProviderConfigurationPanel {
	private static final long serialVersionUID = -2982011824250544454L;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#initializeFromConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void initializeFromConfiguration(MatchingProcessConfiguration configuration) {
		if(configuration == null)
			return;
		
		Set<String> selectedSources = new HashSet<String>();
		
		if(configuration.getSources() != null && configuration.getSources().length > 0) {
			selectedSources.addAll(Arrays.asList(configuration.getSources()));
		}
		
		this._CCSBT.setSelected(selectedSources.contains("CCSBT"));
		this._IATTC.setSelected(selectedSources.contains("IATTC"));
		this._ICCAT.setSelected(selectedSources.contains("ICCAT"));
		this._IOTC.setSelected(selectedSources.contains("IOTC"));
		this._WCPFC.setSelected(selectedSources.contains("WCPFC"));
		
		if(configuration.getSourcesUpdatedAfter() != null) {
			this._enableUpdatedAfter.setSelected(true);
			this._updatedAfter.setDate(new Date(configuration.getSourcesUpdatedAfter()));
		}
		
		if(configuration.getSourcesUpdatedBefore() != null) {
			this._enableUpdatedBefore.setSelected(true);
			this._updatedBefore.setDate(new Date(configuration.getSourcesUpdatedBefore()));
		}
 	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#updateConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void updateConfiguration(MatchingProcessConfiguration configuration) {
		if(configuration == null)
			return;
		
		List<String> selectedSources = new ArrayList<String>();
		
		if(this._CCSBT.isSelected())
			selectedSources.add("CCSBT");
		
		if(this._IATTC.isSelected())
			selectedSources.add("IATTC");
		
		if(this._ICCAT.isSelected())
			selectedSources.add("ICCAT");
		
		if(this._IOTC.isSelected())
			selectedSources.add("IOTC");
		
		if(this._WCPFC.isSelected())
			selectedSources.add("WCPFC");
		
		configuration.setSources(selectedSources.toArray(new String[selectedSources.size()]));
		
		if(this._enableUpdatedAfter.isSelected()) {
			configuration.setSourcesUpdatedAfter(this._updatedAfter.getDate().getTime());
		} else {
			configuration.setSourcesUpdatedAfter(null);
		}
		
		if(this._enableUpdatedBefore.isSelected()) {
			configuration.setSourcesUpdatedBefore(this._updatedBefore.getDate().getTime());
		} else {
			configuration.setSourcesUpdatedBefore(null);
		}
	}
}
