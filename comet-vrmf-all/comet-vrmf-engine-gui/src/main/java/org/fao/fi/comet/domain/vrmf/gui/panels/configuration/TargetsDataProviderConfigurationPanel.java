package org.fao.fi.comet.domain.vrmf.gui.panels.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;
import org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel;

public class TargetsDataProviderConfigurationPanel extends AbstractDataProviderConfigurationPanel implements ConfigurablePanel {
	private static final long serialVersionUID = -2982011824250544454L;

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#initializeFromConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void initializeFromConfiguration(MatchingProcessConfiguration configuration) {
		if(configuration == null)
			return;
		
		Set<String> selectedTargets = new HashSet<String>();
		
		if(configuration.getTargets() != null && configuration.getTargets().length > 0) {
			selectedTargets.addAll(Arrays.asList(configuration.getTargets()));
		}
		
		this._CCSBT.setSelected(selectedTargets.contains("CCSBT"));
		this._IATTC.setSelected(selectedTargets.contains("IATTC"));
		this._ICCAT.setSelected(selectedTargets.contains("ICCAT"));
		this._IOTC.setSelected(selectedTargets.contains("IOTC"));
		this._WCPFC.setSelected(selectedTargets.contains("WCPFC"));
		
		if(configuration.getTargetsUpdatedAfter() != null) {
			this._enableUpdatedAfter.setSelected(true);
			this._updatedAfter.setDate(new Date(configuration.getTargetsUpdatedAfter()));
		}
		
		if(configuration.getTargetsUpdatedBefore() != null) {
			this._enableUpdatedBefore.setSelected(true);
			this._updatedBefore.setDate(new Date(configuration.getTargetsUpdatedBefore()));
		}
 	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#updateConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void updateConfiguration(MatchingProcessConfiguration configuration) {
		if(configuration == null)
			return;
		
		List<String> selectedTargets = new ArrayList<String>();
		
		if(this._CCSBT.isSelected())
			selectedTargets.add("CCSBT");
		
		if(this._IATTC.isSelected())
			selectedTargets.add("IATTC");
		
		if(this._ICCAT.isSelected())
			selectedTargets.add("ICCAT");
		
		if(this._IOTC.isSelected())
			selectedTargets.add("IOTC");
		
		if(this._WCPFC.isSelected())
			selectedTargets.add("WCPFC");
		
		configuration.setTargets(selectedTargets.toArray(new String[selectedTargets.size()]));
		
		if(this._enableUpdatedAfter.isSelected()) {
			configuration.setTargetsUpdatedAfter(this._updatedAfter.getDate().getTime());
		} else {
			configuration.setTargetsUpdatedAfter(null);
		}
		
		if(this._enableUpdatedBefore.isSelected()) {
			configuration.setTargetsUpdatedBefore(this._updatedBefore.getDate().getTime());
		} else {
			configuration.setTargetsUpdatedBefore(null);
		}
	}
}
