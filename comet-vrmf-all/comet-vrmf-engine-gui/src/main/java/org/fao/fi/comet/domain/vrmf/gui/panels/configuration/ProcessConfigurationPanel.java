package org.fao.fi.comet.domain.vrmf.gui.panels.configuration;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;
import org.fao.fi.comet.domain.vrmf.gui.panels.AbstractInputPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel;

public class ProcessConfigurationPanel extends AbstractInputPanel implements ConfigurablePanel {
	private static final long serialVersionUID = -1115449197220295506L;

	private JSpinner _maxCandidates;
	private JSpinner _minScore;

	public ProcessConfigurationPanel() {
		this.initialize();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#initializeFromConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void initializeFromConfiguration(MatchingProcessConfiguration configuration) {
		if(configuration == null)
			return;
		
		this._maxCandidates.setValue(configuration.getMaxCandidatesPerEntry());
		this._minScore.setValue(configuration.getMinScore());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#updateConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void updateConfiguration(MatchingProcessConfiguration configuration) {
		if(configuration == null)
			return;
		
		configuration.setMaxCandidatesPerEntry((int)this._maxCandidates.getValue());
		configuration.setMinScore((double)this._minScore.getValue());
	}
	
	@Override
	protected void initialize() {
		this.setLayout(new GridBagLayout());
		
		JLabel lblNewLabel = new JLabel("Max candidates per entry:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(5, 5, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		this.add(lblNewLabel, gbc_lblNewLabel);
		
		_maxCandidates = new JSpinner();
		_maxCandidates.setModel(new SpinnerNumberModel(5, 0, 50, 1));
		GridBagConstraints gbc__maxCandidates = new GridBagConstraints();
		gbc__maxCandidates.fill = GridBagConstraints.HORIZONTAL;
		gbc__maxCandidates.weightx = 5.0;
		gbc__maxCandidates.insets = new Insets(5, 0, 5, 0);
		gbc__maxCandidates.gridx = 1;
		gbc__maxCandidates.gridy = 0;
		this.add(_maxCandidates, gbc__maxCandidates);
		
		JLabel lblNewLabel_1 = new JLabel("Minimum score:");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(5, 10, 5, 5);
		gbc_lblNewLabel_1.gridx = 2;
		gbc_lblNewLabel_1.gridy = 0;
		this.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		_minScore = new JSpinner();
		_minScore.setModel(new SpinnerNumberModel(0.8, 0.0, 1.0, 0.01));
		GridBagConstraints gbc__minScore = new GridBagConstraints();
		gbc__minScore.insets = new Insets(5, 0, 5, 0);
		gbc__minScore.weightx = 5.0;
		gbc__minScore.fill = GridBagConstraints.HORIZONTAL;
		gbc__minScore.gridx = 3;
		gbc__minScore.gridy = 0;
		this.add(_minScore, gbc__minScore);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.weightx = 100.0;
		gbc_horizontalStrut.insets = new Insets(0, 0, 0, 5);
		gbc_horizontalStrut.gridx = 4;
		gbc_horizontalStrut.gridy = 0;
		this.add(horizontalStrut, gbc_horizontalStrut);
	}

	@Override
	public GridBagConstraints getConstraints() {
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.weightx = 100.0;
		constraints.insets = new Insets(0, 0, 5, 0);
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = 2;

		return constraints;
	}

}
