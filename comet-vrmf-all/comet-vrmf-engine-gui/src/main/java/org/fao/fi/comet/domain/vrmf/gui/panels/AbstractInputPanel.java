package org.fao.fi.comet.domain.vrmf.gui.panels;

import java.awt.GridBagConstraints;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.fao.fi.comet.domain.vrmf.gui.support.UIUtils;

abstract public class AbstractInputPanel extends JPanel {
	private static final long serialVersionUID = -4530584073625043660L;
	
	protected final NumberFormat INTEGER_2_FORMATTER = new DecimalFormat("00");
	
	public int asInt(JTextField container) {
		return Integer.parseInt(container.getText());
	}
	
	public double asDouble(JTextField container) {
		return Double.parseDouble(container.getText());
	}
		
	public void setInt(JTextField container, int toSet) {
		container.setText(String.valueOf(toSet));
	}

	public void setDouble(JTextField container, double toSet, int precision) {
		if(precision == Integer.MAX_VALUE)
			container.setText(String.valueOf(toSet));
		
		String pattern = "#0";
		
		if(precision > 0) {
			pattern += ".";
		}
		
		for(int p=0; p<precision; p++)
			pattern += "0";
		
		container.setText(new DecimalFormat(pattern).format(toSet));
	}
	
	public int asInt(JLabel container) {
		return Integer.parseInt(container.getText());
	}
	
	public double asDouble(JLabel container) {
		return Double.parseDouble(container.getText());
	}

	public void setInt(JLabel container, int toSet) {
		container.setText(String.valueOf(toSet));
	}

	public void setDouble(JLabel container, double toSet, int precision) {
		if(precision == Integer.MAX_VALUE)
			container.setText(String.valueOf(toSet));
		
		String pattern = "#0";
		
		if(precision > 0) {
			pattern += ".";
			
			do {
				pattern += "0";
			} while(precision-- > 0);
		}
		
		container.setText(new DecimalFormat(pattern).format(toSet));
	}
	
	public void enableInputs() {
		UIUtils.enableInputs(this);
	}
	
	public void disableInputs() {
		UIUtils.disableInputs(this);
	}
	
	abstract protected void initialize();
//	abstract protected void dispose();
	
	abstract public GridBagConstraints getConstraints();
}
