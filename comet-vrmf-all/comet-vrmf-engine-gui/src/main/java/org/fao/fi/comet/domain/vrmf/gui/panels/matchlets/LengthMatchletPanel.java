package org.fao.fi.comet.domain.vrmf.gui.panels.matchlets;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JToggleButton;

import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.model.matchlets.MatchletConfigurationParameter;
import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;

public class LengthMatchletPanel extends AbstractMatchletPanel {
	private static final long serialVersionUID = -4117474383272210139L;
	
	private JToggleButton _lengthMatcherForceType;

	public LengthMatchletPanel() {
		this.initialize();
	}
	
	@Override
	protected void initialize() {
		super.initialize();
		
		this._matcherWeight.setValue(20);
		this._matcherScore.setValue(70);

		this.addMissingComponents(6);
			
		_lengthMatcherForceType = new JToggleButton("Force type comparison");
		GridBagConstraints gbc__lengthMatcherForceType = new GridBagConstraints();
		gbc__lengthMatcherForceType.insets = new Insets(0, 0, 5, 0);
		gbc__lengthMatcherForceType.gridx = 0;
		gbc__lengthMatcherForceType.gridy = 5;
		this.add(_lengthMatcherForceType, gbc__lengthMatcherForceType);
	}
	
	@Override
	protected boolean isEnabledByDefault() {
		return false;
	}
	
	@Override
	protected boolean isOptionalByDefault() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#initializeFromConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void initializeFromConfiguration(MatchingProcessConfiguration configuration) {
		MatchletConfiguration current = this.findConfigurationFor("LengthHistoricalMatchlet", configuration);
		
		super.commonInitializeFromConfiguration(current);
		
		if(current != null) {
			MatchletConfigurationParameter excludeTypesComparison = this.findConfigurationParameterFor("excludeTypesComparison", current);
			if(excludeTypesComparison.getActualValue() != null) {
				this._lengthMatcherForceType.setSelected(
					excludeTypesComparison.getActualValue() == null || 
					Boolean.FALSE.equals(Boolean.parseBoolean(excludeTypesComparison.getValue()))
				);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#updateConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void updateConfiguration(MatchingProcessConfiguration processConfiguration) {
		MatchletConfiguration currentConfiguration = super.commonUpdateConfiguration("LengthHistoricalMatchlet", processConfiguration);

		if(currentConfiguration != null) {
			currentConfiguration.with("excludeTypesComparison", !this._lengthMatcherForceType.isSelected());
		}
	}
}
