package org.fao.fi.comet.domain.vrmf.gui.panels.matchlets;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JToggleButton;

import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.model.matchlets.MatchletConfigurationParameter;
import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;
import org.fao.fi.comet.domain.vrmf.gui.panels.AbstractInputPanel;
import org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel;

abstract public class AbstractMatchletPanel extends AbstractInputPanel implements ConfigurablePanel {
	private static final long serialVersionUID = 7774355955428969510L;
	
	protected JToggleButton _matcherEnabled;
	protected JLabel _matcherWeightLabel;
	protected JSlider _matcherWeight;
	protected JLabel _matcherScoreLabel;
	protected JSlider _matcherScore;
	protected JToggleButton _matcherOptional;
	
	@SuppressWarnings("rawtypes")
	protected Map<String, Class<? extends Matchlet>> _matchletsMap = new HashMap<String, Class<? extends Matchlet>>();
	
	@SuppressWarnings("rawtypes")
	public AbstractMatchletPanel() {
		ServiceLoader<? extends Matchlet> loader = ServiceLoader.load(Matchlet.class);
		
		for(Matchlet matchlet : loader) {
			this._matchletsMap.put(matchlet.getName(), matchlet.getClass());
		}
	}
	
	@Override
	protected void initialize() {
		this.setLayout(new GridBagLayout());
		
		this._matcherEnabled = new JToggleButton("Enabled");
		this._matcherEnabled.setSelected(this.isEnabledByDefault());
		
		GridBagConstraints gbcFlagMatcherEnabled = new GridBagConstraints();
		gbcFlagMatcherEnabled.insets = new Insets(0, 0, 5, 0);
		gbcFlagMatcherEnabled.gridx = 0;
		gbcFlagMatcherEnabled.gridy = 0;
		this.add(this._matcherEnabled, gbcFlagMatcherEnabled);
		
		this._matcherWeightLabel = new JLabel("Weight:");
		this._matcherWeightLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.anchor = GridBagConstraints.WEST;
		gbc_label_1.insets = new Insets(0, 0, 5, 0);
		gbc_label_1.gridx = 0;
		gbc_label_1.gridy = 1;
		this.add(this._matcherWeightLabel, gbc_label_1);
		
		this._matcherWeight = new JSlider();
		this._matcherWeight.setPaintTicks(true);
		this._matcherWeight.setPaintLabels(true);
		this._matcherWeight.setMinorTickSpacing(5);
		this._matcherWeight.setMajorTickSpacing(10);
		GridBagConstraints gbcFlagMatcherWeight = new GridBagConstraints();
		gbcFlagMatcherWeight.anchor = GridBagConstraints.WEST;
		gbcFlagMatcherWeight.insets = new Insets(0, 0, 5, 0);
		gbcFlagMatcherWeight.gridx = 0;
		gbcFlagMatcherWeight.gridy = 2;
		this.add(this._matcherWeight, gbcFlagMatcherWeight);
		
		this._matcherScoreLabel = new JLabel("Min. score:");
		this._matcherScoreLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.anchor = GridBagConstraints.WEST;
		gbc_label_2.insets = new Insets(0, 0, 5, 0);
		gbc_label_2.gridx = 0;
		gbc_label_2.gridy = 3;
		this.add(this._matcherScoreLabel, gbc_label_2);
		
		this._matcherScore = new JSlider();
		this._matcherScore.setPaintTicks(true);
		this._matcherScore.setPaintLabels(true);
		this._matcherScore.setMinorTickSpacing(5);
		this._matcherScore.setMajorTickSpacing(10);
		GridBagConstraints gbcFlagMatcherScore = new GridBagConstraints();
		gbcFlagMatcherScore.anchor = GridBagConstraints.WEST;
		gbcFlagMatcherScore.insets = new Insets(0, 0, 5, 0);
		gbcFlagMatcherScore.gridx = 0;
		gbcFlagMatcherScore.gridy = 4;
		this.add(this._matcherScore, gbcFlagMatcherScore);
	}

	@Override
	public GridBagConstraints getConstraints() {
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 0);
		constraints.weighty = 100.0;
		constraints.weightx = 100.0;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.gridx = 0;
		constraints.gridy = 0;
		
		return constraints;
	}
	
	abstract protected boolean isEnabledByDefault();
	abstract protected boolean isOptionalByDefault();
	
	protected void addMissingComponents(int position) {
		this.fillSpace(position);
		this.addOptionalButton(position+1);
	}
	
	protected void fillSpace(int position) {
		Component verticalStrut_1 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_1 = new GridBagConstraints();
		gbc_verticalStrut_1.weighty = 100.0;
		gbc_verticalStrut_1.fill = GridBagConstraints.BOTH;
		gbc_verticalStrut_1.anchor = GridBagConstraints.WEST;
		gbc_verticalStrut_1.insets = new Insets(0, 0, 5, 0);
		gbc_verticalStrut_1.gridx = 0;
		gbc_verticalStrut_1.gridy = position;
		this.add(verticalStrut_1, gbc_verticalStrut_1);
	}
	
	protected void addOptionalButton(int position) {
		this._matcherOptional = new JToggleButton("Optional");
		GridBagConstraints gbcFlagMatcherOptional = new GridBagConstraints();
		gbcFlagMatcherOptional.gridx = 0;
		gbcFlagMatcherOptional.gridy = position;
		this.add(this._matcherOptional, gbcFlagMatcherOptional);
		this._matcherOptional.setSelected(this.isOptionalByDefault());
	}
	
	protected MatchletConfiguration findConfigurationFor(String matchlet, MatchingProcessConfiguration configuration) {
		Collection<MatchletConfiguration> matchlets = configuration.getMatchletConfigurations();
		
		if(matchlets == null || matchlets.isEmpty())
			return null;
		
		for(MatchletConfiguration in : matchlets) {
			if(in.getMatchletName().equals(matchlet))
				return in;
		}
		
		return null;
	}
	
	protected MatchletConfigurationParameter findConfigurationParameterFor(String parameterName, MatchletConfiguration configuration) {
		Collection<MatchletConfigurationParameter> parameters = configuration.getMatchletParameters();
		
		if(parameters == null || parameters.isEmpty())
			return null;
		
		for(MatchletConfigurationParameter in : parameters) {
			if(in.getName().equals(parameterName))
				return in;
		}
		
		return null;
	}
	
	protected void commonInitializeFromConfiguration(MatchletConfiguration current) {
		this._matcherEnabled.setSelected(current != null);

		if(current != null) {
			MatchletConfigurationParameter isOptional = this.findConfigurationParameterFor(Matchlet.OPTIONAL_PARAM, current);
			this._matcherOptional.setSelected(isOptional != null && Boolean.parseBoolean(isOptional.getValue()));
			
			MatchletConfigurationParameter weight = this.findConfigurationParameterFor(Matchlet.WEIGHT_PARAM, current);
			if(weight.getActualValue() != null) {
				this._matcherWeight.setValue((int)(Math.round(Double.parseDouble(weight.getValue()))));
			}
			
			MatchletConfigurationParameter score = this.findConfigurationParameterFor(Matchlet.MIN_SCORE_PARAM, current);
			if(score.getActualValue() != null) {
				this._matcherScore.setValue((int)(Math.round(Double.parseDouble(score.getValue()) * 100.0)));
			}
		}
	}
	
	protected MatchletConfiguration commonUpdateConfiguration(String matchlet, MatchingProcessConfiguration processConfiguration) {
		if(processConfiguration == null)
			return null;
		
		if(!this._matcherEnabled.isSelected())
			return null;
		
		MatchletConfiguration current = this.findConfigurationFor(matchlet, processConfiguration);
		
		if(current == null) {
			current = new MatchletConfiguration(matchlet);
			
			if(processConfiguration.getMatchletConfigurations() == null)
				processConfiguration.setMatchletConfigurations(new ArrayList<MatchletConfiguration>());
			
			processConfiguration.getMatchletConfigurations().add(current);
		} 
		
		current.setMatchletType(this._matchletsMap.get(matchlet).getName());

		current.with(Matchlet.WEIGHT_PARAM, this._matcherWeight.getValue());
		current.with(Matchlet.MIN_SCORE_PARAM, this._matcherScore.getValue() * 0.01D);
		current.with(Matchlet.OPTIONAL_PARAM, this._matcherOptional.isSelected());
	
		return current;
	}
}
