/**
 * 
 */
package org.fao.fi.comet.domain.vrmf.gui.support;

import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JToggleButton;
import javax.swing.text.JTextComponent;

import org.fao.fi.comet.domain.vrmf.gui.panels.AbstractInputPanel;
import org.jdesktop.swingx.JXDatePicker;

/**
 * @author ffiorellato
 */
public class UIUtils {
	static private boolean UI_ENABLED = true;
	
	static final public void error(Component parent, String title, String text) {
		UIUtils.popup(parent, title, text, JOptionPane.ERROR_MESSAGE);
	}
	
	static final public void info(Component parent, String title, String text) {
		UIUtils.popup(parent, title, text, JOptionPane.INFORMATION_MESSAGE);
	}
	
	static final public void warning(Component parent, String title, String text) {
		UIUtils.popup(parent, title, text, JOptionPane.WARNING_MESSAGE);
	}
	
	static final public void popup(Component parent, String title, String text, int type) {
		JOptionPane.showMessageDialog(parent, text, title, type);
	}
	
	static final public void enableInputs(JComponent container) {
		if(!UI_ENABLED) {
			UIUtils.setInputState(container, true);
		}
	}
	
	static final public void disableInputs(JComponent container) {
		if(UI_ENABLED)
			UIUtils.setInputState(container, false);
	}
	
	static final protected void setInputState(JComponent owner, boolean enabled) {
		UI_ENABLED = enabled;
		
		boolean isTabbedPane = JTabbedPane.class.isAssignableFrom(owner.getClass());
		
		int numberOfComponents = isTabbedPane ? ((JTabbedPane)owner).getTabCount() : owner.getComponentCount();

		boolean nested = false;
		
		if(numberOfComponents > 0) {
			JComponent current;
			
			for(int c=0; c<numberOfComponents; c++) {
				
				current = (JComponent)(isTabbedPane ? ((JTabbedPane)owner).getComponentAt(c) : ((JComponent)owner).getComponent(c));
				
				if(UIUtils.isInputComponent(current))
					current.setEnabled(enabled);
				
				nested = AbstractInputPanel.class.isAssignableFrom(current.getClass()) ||
						 JPanel.class.isAssignableFrom(current.getClass()) ||
						 JTabbedPane.class.isAssignableFrom(current.getClass());
				
				if(nested)
					UIUtils.setInputState(current, enabled);
			}
		}
	}
	
	static final protected boolean isInputComponent(JComponent what) {
		final Class<?> componentClass = what.getClass();
		
		return JTextComponent.class.isAssignableFrom(componentClass) ||
			   JComboBox.class.isAssignableFrom(componentClass) ||
			   JRadioButton.class.isAssignableFrom(componentClass) ||
			   JCheckBox.class.isAssignableFrom(componentClass) ||
			   JSlider.class.isAssignableFrom(componentClass) ||
			   JSpinner.class.isAssignableFrom(componentClass) ||
			   JButton.class.isAssignableFrom(componentClass) ||
			   JToggleButton.class.isAssignableFrom(componentClass) ||
			   JXDatePicker.class.isAssignableFrom(componentClass);
	}
}
