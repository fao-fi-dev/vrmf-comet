package org.fao.fi.comet.domain.vrmf.gui.panels.matchlets;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JToggleButton;

import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.model.matchlets.MatchletConfigurationParameter;
import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;

public class TonnageMatchletPanel extends AbstractMatchletPanel {
	private static final long serialVersionUID = -4117474383272210139L;
	
	private JToggleButton _tonnageMatcherForceType;

	public TonnageMatchletPanel() {
		this.initialize();
	}
	
	@Override
	protected void initialize() {
		super.initialize();
		
		this._matcherWeight.setValue(20);
		this._matcherScore.setValue(70);

		this.addMissingComponents(6);
		
		this._tonnageMatcherForceType = new JToggleButton("Force type comparison");
		GridBagConstraints gbc__tonnageMatcherForceType = new GridBagConstraints();
		gbc__tonnageMatcherForceType.insets = new Insets(0, 0, 5, 0);
		gbc__tonnageMatcherForceType.gridx = 0;
		gbc__tonnageMatcherForceType.gridy = 5;
		this.add(this._tonnageMatcherForceType, gbc__tonnageMatcherForceType);
	}
	
	@Override
	protected boolean isEnabledByDefault() {
		return false;
	}
	
	@Override
	protected boolean isOptionalByDefault() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#initializeFromConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void initializeFromConfiguration(MatchingProcessConfiguration configuration) {
		MatchletConfiguration current = this.findConfigurationFor("TonnageHistoricalMatchlet", configuration);
		
		super.commonInitializeFromConfiguration(current);
		
		if(current != null) {
			MatchletConfigurationParameter excludeTypesComparison = this.findConfigurationParameterFor("excludeTypesComparison", current);
			if(excludeTypesComparison.getActualValue() != null) {
				this._tonnageMatcherForceType.setSelected(
					excludeTypesComparison.getActualValue() == null || 
					Boolean.FALSE.equals(Boolean.parseBoolean(excludeTypesComparison.getValue()))
				);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#updateConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void updateConfiguration(MatchingProcessConfiguration processConfiguration) {
		MatchletConfiguration currentConfiguration = super.commonUpdateConfiguration("TonnageHistoricalMatchlet", processConfiguration);
		
		if(currentConfiguration != null) {
			currentConfiguration.with("excludeTypesComparison", !this._tonnageMatcherForceType.isSelected());
		}
	}
}
