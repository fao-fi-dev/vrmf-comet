/**
 * 
 */
package org.fao.fi.comet.domain.vrmf.gui.panels.process;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 * @author ffiorellato
 */
public class RuntimePanel extends JPanel {
	private static final long serialVersionUID = 336093651153902649L;
	
	private JLabel _statusText;
	private JLabel _xms;
	private JLabel _xmx;
	private JProgressBar _heap;
	public RuntimePanel() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		setLayout(gridBagLayout);
		
		_statusText = new JLabel("Idle...");
		GridBagConstraints gbc__statusText = new GridBagConstraints();
		gbc__statusText.fill = GridBagConstraints.HORIZONTAL;
		gbc__statusText.anchor = GridBagConstraints.WEST;
		gbc__statusText.weightx = 100.0;
		gbc__statusText.insets = new Insets(5, 5, 5, 5);
		gbc__statusText.gridx = 0;
		gbc__statusText.gridy = 0;
		add(_statusText, gbc__statusText);
		
		JPanel _memoryPanel = new JPanel();
		GridBagConstraints gbc__memoryPanel = new GridBagConstraints();
		gbc__memoryPanel.weightx = 10.0;
		gbc__memoryPanel.anchor = GridBagConstraints.EAST;
		gbc__memoryPanel.insets = new Insets(5, 5, 5, 5);
		gbc__memoryPanel.gridx = 1;
		gbc__memoryPanel.gridy = 0;
		add(_memoryPanel, gbc__memoryPanel);
		GridBagLayout gbl__memoryPanel = new GridBagLayout();
		_memoryPanel.setLayout(gbl__memoryPanel);
		
		JLabel lblNewLabel = new JLabel("Memory:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(0, 5, 0, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		_memoryPanel.add(lblNewLabel, gbc_lblNewLabel);
		
		_xms = new JLabel("0");
		GridBagConstraints gbc__xms = new GridBagConstraints();
		gbc__xms.fill = GridBagConstraints.HORIZONTAL;
		gbc__xms.weightx = 5.0;
		gbc__xms.anchor = GridBagConstraints.WEST;
		gbc__xms.insets = new Insets(0, 5, 0, 5);
		gbc__xms.gridx = 1;
		gbc__xms.gridy = 0;
		_memoryPanel.add(_xms, gbc__xms);
		
		_heap = new JProgressBar();
		GridBagConstraints gbc__heap = new GridBagConstraints();
		gbc__heap.gridx = 2;
		gbc__heap.gridy = 0;
		_memoryPanel.add(_heap, gbc__heap);
		
		_xmx = new JLabel("0 MB");
		GridBagConstraints gbc__xmx = new GridBagConstraints();
		gbc__xmx.fill = GridBagConstraints.HORIZONTAL;
		gbc__xmx.weightx = 5.0;
		gbc__xmx.anchor = GridBagConstraints.EAST;
		gbc__xmx.insets = new Insets(0, 5, 0, 0);
		gbc__xmx.gridx = 3;
		gbc__xmx.gridy = 0;
		_memoryPanel.add(_xmx, gbc__xmx);
	
		this._heap.setMinimum(0);
		this._heap.setMaximum((int)Runtime.getRuntime().maxMemory());
	}

	public void updateStatus() {
		long max = Runtime.getRuntime().maxMemory();
		long used = max - Runtime.getRuntime().freeMemory();
		
		double maxMB = max * 1.0 / 1024.0 / 1024.0;
		
		this._heap.setMaximum((int)max);
		this._heap.setValue((int)used);

		this._xmx.setText(new DecimalFormat("0.00").format(maxMB) + " MB");
	}
	
	public void updateStatusText(String text) {
		this._statusText.setText(text);
	}
}