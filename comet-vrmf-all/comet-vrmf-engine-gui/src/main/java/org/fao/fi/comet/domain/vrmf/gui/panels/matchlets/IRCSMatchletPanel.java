package org.fao.fi.comet.domain.vrmf.gui.panels.matchlets;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JSlider;

import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.model.matchlets.MatchletConfigurationParameter;
import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;

public class IRCSMatchletPanel extends AbstractMatchletPanel {
	private static final long serialVersionUID = -4117474383272210139L;
	
	private JSlider _ircsMatcherCountryWeight;
	private JSlider _ircsMatcherIRCSWeight;
	
	public IRCSMatchletPanel() {
		this.initialize();
	}
	
	@Override
	protected void initialize() {
		super.initialize();
		
		this._matcherWeight.setValue(30);
		this._matcherScore.setValue(65);
			
		JLabel label_7 = new JLabel("Country weight:");
		label_7.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_label_7 = new GridBagConstraints();
		gbc_label_7.anchor = GridBagConstraints.WEST;
		gbc_label_7.insets = new Insets(0, 0, 5, 0);
		gbc_label_7.gridx = 0;
		gbc_label_7.gridy = 5;
		this.add(label_7, gbc_label_7);
		
		this._ircsMatcherCountryWeight = new JSlider();
		this._ircsMatcherCountryWeight.setValue(25);
		this._ircsMatcherCountryWeight.setPaintTicks(true);
		this._ircsMatcherCountryWeight.setPaintLabels(true);
		this._ircsMatcherCountryWeight.setMinorTickSpacing(5);
		this._ircsMatcherCountryWeight.setMajorTickSpacing(10);
		GridBagConstraints gbc__ircsMatcherCountryWeight = new GridBagConstraints();
		gbc__ircsMatcherCountryWeight.insets = new Insets(0, 0, 5, 0);
		gbc__ircsMatcherCountryWeight.gridx = 0;
		gbc__ircsMatcherCountryWeight.gridy = 6;
		this.add(this._ircsMatcherCountryWeight, gbc__ircsMatcherCountryWeight);
		
		JLabel label_8 = new JLabel("IRCS weight:");
		label_8.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_label_8 = new GridBagConstraints();
		gbc_label_8.anchor = GridBagConstraints.WEST;
		gbc_label_8.insets = new Insets(0, 0, 5, 0);
		gbc_label_8.gridx = 0;
		gbc_label_8.gridy = 7;
		this.add(label_8, gbc_label_8);
		
		this._ircsMatcherIRCSWeight = new JSlider();
		this._ircsMatcherIRCSWeight.setValue(100);
		this._ircsMatcherIRCSWeight.setPaintTicks(true);
		this._ircsMatcherIRCSWeight.setPaintLabels(true);
		this._ircsMatcherIRCSWeight.setMinorTickSpacing(5);
		this._ircsMatcherIRCSWeight.setMajorTickSpacing(10);
		GridBagConstraints gbc__ircsMatcherIRCSWeight = new GridBagConstraints();
		gbc__ircsMatcherIRCSWeight.insets = new Insets(0, 0, 5, 0);
		gbc__ircsMatcherIRCSWeight.gridx = 0;
		gbc__ircsMatcherIRCSWeight.gridy = 8;
		this.add(this._ircsMatcherIRCSWeight, gbc__ircsMatcherIRCSWeight);

		this.addMissingComponents(10);
	}
	
	@Override
	protected boolean isEnabledByDefault() {
		return true;
	}
	
	@Override
	protected boolean isOptionalByDefault() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#initializeFromConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void initializeFromConfiguration(MatchingProcessConfiguration configuration) {
		MatchletConfiguration current = this.findConfigurationFor("IRCSHistoricalMatchlet", configuration);
		
		super.commonInitializeFromConfiguration(current);
		
		if(current != null) {
			MatchletConfigurationParameter countryWeight = this.findConfigurationParameterFor("countryWeight", current);
			if(countryWeight.getActualValue() != null) {
				this._ircsMatcherCountryWeight.setValue((int)(Math.round(Double.parseDouble(countryWeight.getValue()))));
			}
			
			MatchletConfigurationParameter ircsWeight = this.findConfigurationParameterFor("ircsWeight", current);
			if(ircsWeight.getActualValue() != null) {
				this._ircsMatcherIRCSWeight.setValue((int)(Math.round(Double.parseDouble(ircsWeight.getValue()))));
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#updateConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void updateConfiguration(MatchingProcessConfiguration processConfiguration) {
		MatchletConfiguration currentConfiguration = super.commonUpdateConfiguration("IRCSHistoricalMatchlet", processConfiguration);
		
		if(currentConfiguration != null) {
			currentConfiguration.with("countryWeight", this._ircsMatcherCountryWeight.getValue());
			currentConfiguration.with("ircsWeight", this._ircsMatcherIRCSWeight.getValue());
		}
	}
}
