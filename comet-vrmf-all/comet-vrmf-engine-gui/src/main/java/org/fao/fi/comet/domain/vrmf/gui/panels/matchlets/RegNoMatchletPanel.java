package org.fao.fi.comet.domain.vrmf.gui.panels.matchlets;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JSlider;

import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.model.matchlets.MatchletConfigurationParameter;
import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;

public class RegNoMatchletPanel extends AbstractMatchletPanel {
	private static final long serialVersionUID = -4117474383272210139L;
	
	private JSlider _regNoMatcherCountryWeight;
	private JSlider _regNoMatcherPortWeight;
	private JSlider _regNoMatcherRegNoWeight;
	
	public RegNoMatchletPanel() {
		this.initialize();
	}
	
	@Override
	protected void initialize() {
		super.initialize();
		
		this._matcherWeight.setValue(30);
		this._matcherScore.setValue(65);
		
		JLabel label_7 = new JLabel("Country weight:");
		label_7.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_label_7 = new GridBagConstraints();
		gbc_label_7.anchor = GridBagConstraints.WEST;
		gbc_label_7.insets = new Insets(0, 0, 5, 0);
		gbc_label_7.gridx = 0;
		gbc_label_7.gridy = 5;
		this.add(label_7, gbc_label_7);
		
		this._regNoMatcherCountryWeight = new JSlider();
		this._regNoMatcherCountryWeight.setValue(25);
		this._regNoMatcherCountryWeight.setPaintTicks(true);
		this._regNoMatcherCountryWeight.setPaintLabels(true);
		this._regNoMatcherCountryWeight.setMinorTickSpacing(5);
		this._regNoMatcherCountryWeight.setMajorTickSpacing(10);
		GridBagConstraints gbc__regNoMatcherCountryWeight = new GridBagConstraints();
		gbc__regNoMatcherCountryWeight.insets = new Insets(0, 0, 5, 0);
		gbc__regNoMatcherCountryWeight.gridx = 0;
		gbc__regNoMatcherCountryWeight.gridy = 6;
		this.add(this._regNoMatcherCountryWeight, gbc__regNoMatcherCountryWeight);
		
		JLabel label_12 = new JLabel("Port weight:");
		label_12.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_label_12 = new GridBagConstraints();
		gbc_label_12.anchor = GridBagConstraints.WEST;
		gbc_label_12.insets = new Insets(0, 0, 5, 0);
		gbc_label_12.gridx = 0;
		gbc_label_12.gridy = 7;
		this.add(label_12, gbc_label_12);
		
		this._regNoMatcherPortWeight = new JSlider();
		this._regNoMatcherPortWeight.setValue(0);
		this._regNoMatcherPortWeight.setPaintTicks(true);
		this._regNoMatcherPortWeight.setPaintLabels(true);
		this._regNoMatcherPortWeight.setMinorTickSpacing(5);
		this._regNoMatcherPortWeight.setMajorTickSpacing(10);
		GridBagConstraints gbc__regNoMatcherPortWeight = new GridBagConstraints();
		gbc__regNoMatcherPortWeight.insets = new Insets(0, 0, 5, 0);
		gbc__regNoMatcherPortWeight.gridx = 0;
		gbc__regNoMatcherPortWeight.gridy = 8;
		this.add(this._regNoMatcherPortWeight, gbc__regNoMatcherPortWeight);
		
		JLabel label_13 = new JLabel("Reg. No. weight:");
		label_13.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_label_13 = new GridBagConstraints();
		gbc_label_13.anchor = GridBagConstraints.WEST;
		gbc_label_13.insets = new Insets(0, 0, 5, 0);
		gbc_label_13.gridx = 0;
		gbc_label_13.gridy = 9;
		this.add(label_13, gbc_label_13);
		
		this._regNoMatcherRegNoWeight = new JSlider();
		this._regNoMatcherRegNoWeight.setValue(100);
		this._regNoMatcherRegNoWeight.setPaintTicks(true);
		this._regNoMatcherRegNoWeight.setPaintLabels(true);
		this._regNoMatcherRegNoWeight.setMinorTickSpacing(5);
		this._regNoMatcherRegNoWeight.setMajorTickSpacing(10);
		
		GridBagConstraints gbc__regNoMatcherRegNoWeight = new GridBagConstraints();
		gbc__regNoMatcherRegNoWeight.insets = new Insets(0, 0, 5, 0);
		gbc__regNoMatcherRegNoWeight.gridx = 0;
		gbc__regNoMatcherRegNoWeight.gridy = 10;
		this.add(this._regNoMatcherRegNoWeight, gbc__regNoMatcherRegNoWeight);

		this.addMissingComponents(11);
	}
	
	@Override
	protected boolean isEnabledByDefault() {
		return true;
	}
	
	@Override
	protected boolean isOptionalByDefault() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#initializeFromConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void initializeFromConfiguration(MatchingProcessConfiguration configuration) {
		MatchletConfiguration current = this.findConfigurationFor("RegistrationNoHistoricalMatchlet", configuration);
		
		super.commonInitializeFromConfiguration(current);
		
		if(current != null) {
			MatchletConfigurationParameter countryWeight = this.findConfigurationParameterFor("portCountryWeight", current);
			if(countryWeight.getActualValue() != null) {
				this._regNoMatcherCountryWeight.setValue((int)(Math.round(Double.parseDouble(countryWeight.getValue()))));
			}
			
			MatchletConfigurationParameter portWeight = this.findConfigurationParameterFor("portWeight", current);
			if(portWeight.getActualValue() != null) {
				this._regNoMatcherPortWeight.setValue((int)(Math.round(Double.parseDouble(portWeight.getValue()))));
			}
			
			MatchletConfigurationParameter regNoWeight = this.findConfigurationParameterFor("registrationNumberWeight", current);
			if(regNoWeight.getActualValue() != null) {
				this._regNoMatcherRegNoWeight.setValue((int)(Math.round(Double.parseDouble(regNoWeight.getValue()))));
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#updateConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void updateConfiguration(MatchingProcessConfiguration processConfiguration) {
		MatchletConfiguration currentConfiguration = super.commonUpdateConfiguration("RegistrationNoHistoricalMatchlet", processConfiguration);
		
		if(currentConfiguration != null) {
			currentConfiguration.with("portCountryWeight", this._regNoMatcherCountryWeight.getValue());
			currentConfiguration.with("portWeight", this._regNoMatcherPortWeight.getValue());
			currentConfiguration.with("registrationNumberWeight", this._regNoMatcherRegNoWeight.getValue());
		}
	}
}
