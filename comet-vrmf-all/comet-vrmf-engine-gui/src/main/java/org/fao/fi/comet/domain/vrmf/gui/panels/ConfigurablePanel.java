package org.fao.fi.comet.domain.vrmf.gui.panels;

import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;


/**
 * @author ffiorellato
 *
 */
public interface ConfigurablePanel {
	void updateConfiguration(MatchingProcessConfiguration configuration);
	void initializeFromConfiguration(MatchingProcessConfiguration configuration);
}
