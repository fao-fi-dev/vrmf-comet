package org.fao.fi.comet.domain.vrmf.gui.panels.matchlets;

import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;


public class FlagMatchletPanel extends AbstractMatchletPanel {
	private static final long serialVersionUID = -4117474383272210139L;

	public FlagMatchletPanel() {
		this.initialize();
	}
	
	@Override
	protected void initialize() {
		super.initialize();
	
		this._matcherWeight.setValue(20);
		this._matcherScore.setValue(100);

		this.addMissingComponents(5);
	}
	
	@Override
	protected boolean isEnabledByDefault() {
		return true;
	}
	
	@Override
	protected boolean isOptionalByDefault() {
		return false;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#initializeFromConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void initializeFromConfiguration(MatchingProcessConfiguration configuration) {
		super.commonInitializeFromConfiguration(this.findConfigurationFor("FlagHistoricalMatchlet", configuration));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.gui.panels.ConfigurablePanel#updateConfiguration(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@Override
	public void updateConfiguration(MatchingProcessConfiguration configuration) {
		super.commonUpdateConfiguration("FlagHistoricalMatchlet", configuration);
	}
}
