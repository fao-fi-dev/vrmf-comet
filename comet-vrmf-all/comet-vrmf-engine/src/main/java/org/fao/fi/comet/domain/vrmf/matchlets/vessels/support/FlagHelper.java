/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.support;

import java.io.Serializable;

import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.sh.utility.topology.helpers.TransitiveMappedDataManager;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jul 2010
 */
final public class FlagHelper implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1859180320020044467L;

	public FlagHelper() {
		super();
	}
	

	public MatchingScore computeLikeliness(TransitiveMappedDataManager<Integer, SCountries> countriesMapper, VesselsToFlags firstData, VesselsToFlags secondData) {
		Integer firstFlag = firstData.getCountryId();
		Integer secondFlag = secondData.getCountryId();

		if(firstFlag == null && secondFlag == null)
			return MatchingScore.getNonPerformedTemplate();
		
		if((firstFlag != null && secondFlag == null) ||
		   (firstFlag == null && secondFlag != null))
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();
		
		if(firstFlag.equals(secondFlag))
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();
		
		if(countriesMapper != null && countriesMapper.maps(firstFlag, secondFlag)) {
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();
		}

		return MatchingScore.getNonAuthoritativeNoMatchTemplate();
	}
}
