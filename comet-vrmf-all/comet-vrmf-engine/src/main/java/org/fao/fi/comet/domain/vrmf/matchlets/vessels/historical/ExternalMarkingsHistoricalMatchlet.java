/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.historical;

import java.util.Collection;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletData;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.domain.vrmf.matchlets.vessels.support.ExternalMarkingsHelper;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToExternalMarkings;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Jul 2010
 */
@MatchletDefaultSerializationExclusionPolicy(MatchingSerializationExclusionPolicy.NON_PERFORMED)
@MatchletData(dataType={ VesselsToExternalMarkings.class })
public class ExternalMarkingsHistoricalMatchlet extends AbstractUHistoricalMatchlet<ExtendedVessel, VesselsToExternalMarkings> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5051549998242880045L;
	
	/** The external markings helper reference */
	private ExternalMarkingsHelper _externalMarkingsHelper = new ExternalMarkingsHelper();
	
	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public ExternalMarkingsHistoricalMatchlet() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return ExternalMarkingsHistoricalMatchlet.class.getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ExtendedVessel source, DataIdentifier sourceIdentifier, VesselsToExternalMarkings sourceData, ExtendedVessel target, DataIdentifier targetIdentifier, VesselsToExternalMarkings targetData) {
		return this._externalMarkingsHelper.computeLikeliness(sourceData, targetData);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UVectorialMatchletSkeleton#doExtractData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected Collection<VesselsToExternalMarkings> doExtractData(ExtendedVessel entity, DataIdentifier dataIdentifier) {
		return entity.getExternalMarkingsData();
	}
}