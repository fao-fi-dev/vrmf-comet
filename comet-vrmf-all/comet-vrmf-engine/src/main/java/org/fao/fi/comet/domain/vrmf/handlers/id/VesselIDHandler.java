/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.vrmf.handlers.id;

import org.fao.fi.comet.core.patterns.handlers.id.IDHandler;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Jun 2013
 */
final public class VesselIDHandler implements IDHandler<ExtendedVessel, Integer> {
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.IDHandler#getId(java.lang.Object)
	 */
	@Override
	public Integer getId(final ExtendedVessel data) {
		return data == null ? null : data.getId();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.IDHandler#serializeId(java.io.Serializable)
	 */
	@Override
	public String serializeId(final Integer id) {
		return id == null ? null : id.toString();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.IDHandler#getSerializedId(java.lang.Object)
	 */
	@Override
	public String getSerializedId(final ExtendedVessel data) {
		return this.serializeId(this.getId(data));
	}
}
