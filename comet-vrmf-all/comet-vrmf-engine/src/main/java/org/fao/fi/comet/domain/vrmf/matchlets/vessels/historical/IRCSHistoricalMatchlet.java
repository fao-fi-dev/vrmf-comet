/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.historical;

import java.util.Collection;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletData;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleRangeFrom;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.domain.vrmf.matchlets.vessels.support.IRCSHelper;
import org.fao.fi.sh.utility.topology.helpers.TransitiveMappedDataManager;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Jul 2010
 */
@MatchletDefaultSerializationExclusionPolicy(MatchingSerializationExclusionPolicy.NON_PERFORMED)
@MatchletData(dataType={ VesselsToCallsigns.class })
public class IRCSHistoricalMatchlet extends AbstractUHistoricalMatchlet<ExtendedVessel, VesselsToCallsigns> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5051549998242880045L;
	
	/** The IRCS helper reference */
	private IRCSHelper _IRCSHelper = new IRCSHelper();
	
	@MatchletParameter(name="countryWeight")
	@DoubleRangeFrom(value=0, include=true)
	private double _countryWeight = 5D;

	@MatchletParameter(name="ircsWeight")
	@DoubleRangeFrom(value=0, include=true)
	private double _ircsWeight = 20D;
	
	@MatchletParameter(name="countriesMapper")
	transient private TransitiveMappedDataManager<Integer, SCountries> _countriesMapper;
	
	/**
	 * Class constructor
	 *
	 */
	public IRCSHistoricalMatchlet() {
		super();
	}
	
	/**
	 * @return the 'countryWeight' value
	 */
	public double getCountryWeight() {
		return this._countryWeight;
	}

	/**
	 * @param countryWeight the 'countryWeight' value to set
	 */
	public void setCountryWeight(double countryWeight) {
		this._countryWeight = countryWeight;
	}

	/**
	 * @return the 'ircsWeight' value
	 */
	public double getIrcsWeight() {
		return this._ircsWeight;
	}

	/**
	 * @param ircsWeight the 'ircsWeight' value to set
	 */
	public void setIrcsWeight(double ircsWeight) {
		this._ircsWeight = ircsWeight;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return IRCSHistoricalMatchlet.class.getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ExtendedVessel source, DataIdentifier sourceIdentifier, VesselsToCallsigns sourceData, ExtendedVessel target, DataIdentifier targetIdentifier, VesselsToCallsigns targetData) {
		return this._IRCSHelper.computeLikeliness(this._countriesMapper, this._countryWeight, this._ircsWeight, sourceData, targetData);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UVectorialMatchletSkeleton#doExtractData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected Collection<VesselsToCallsigns> doExtractData(ExtendedVessel entity, DataIdentifier dataIdentifier) {
		return entity.getCallsignData();
	}	
}