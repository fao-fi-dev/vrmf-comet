/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.historical;

import java.util.Collection;

import org.fao.fi.comet.core.exceptions.MatchletConfigurationException;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletData;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.comet.extras.uniform.matchlets.skeleton.historical.UTypedValuedHistoricalMatchlet;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Jul 2010
 */
@MatchletDefaultSerializationExclusionPolicy(MatchingSerializationExclusionPolicy.NON_PERFORMED)
@MatchletData(dataType={ VesselsToLengths.class })
final public class LengthHistoricalMatchlet extends UTypedValuedHistoricalMatchlet<ExtendedVessel, VesselsToLengths> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5051549998242880045L;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return LengthHistoricalMatchlet.class.getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ExtendedVessel source, DataIdentifier sourceIdentifier, VesselsToLengths sourceData, ExtendedVessel target, DataIdentifier targetIdentifier, VesselsToLengths targetData) {
		final String firstLengthType = sourceData.getTypeId();
		final String secondLengthType = targetData.getTypeId();

		final Float firstLength = sourceData.getValue();
		final Float secondLength = targetData.getValue();

		final boolean equalLengthTypes = firstLengthType != null && secondLengthType != null && firstLengthType.equals(secondLengthType);
		final boolean equalLengthValues = equalLengthTypes && firstLength != null && secondLength != null && Double.compare(firstLength, secondLength) == 0; 

		if(firstLengthType == null && secondLengthType == null)
			return MatchingScore.getNonPerformedTemplate();
		
		if(firstLengthType == null || secondLengthType == null)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();

		if(firstLength == null && secondLength == null)
			return MatchingScore.getNonPerformedTemplate();
		
		if(firstLength == null || secondLength == null)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();
		
		if(equalLengthTypes && equalLengthValues)		
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();
		
		if(equalLengthTypes || this._excludeTypesComparison)
			return new MatchingScore(Math.min(firstLength, secondLength) / Math.max(firstLength, secondLength), MatchingType.NON_AUTHORITATIVE);
		
		return MatchingScore.getNonAuthoritativeNoMatchTemplate();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.uniform.extras.matchlets.skeleton.historical.UHistoricalMatchletSkeleton#extractData(java.io.Serializable, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected Collection<VesselsToLengths> extractData(ExtendedVessel entity, DataIdentifier dataIdentifier) {
		return entity.getLengthData();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.MatchletSkeleton#doValidateConfiguration()
	 */
	@Override
	protected void doValidateConfiguration() throws MatchletConfigurationException {
		return;
	}
}