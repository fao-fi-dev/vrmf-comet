/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.historical;

import java.util.Collection;

import org.fao.fi.comet.core.exceptions.MatchletConfigurationException;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletData;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.comet.extras.uniform.matchlets.skeleton.historical.UTypedValuedHistoricalMatchlet;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Jul 2010
 */
@MatchletDefaultSerializationExclusionPolicy(MatchingSerializationExclusionPolicy.NON_PERFORMED)
@MatchletData(dataType={ VesselsToTonnage.class })
final public class TonnageHistoricalMatchlet extends UTypedValuedHistoricalMatchlet<ExtendedVessel, VesselsToTonnage> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5051549998242880045L;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return TonnageHistoricalMatchlet.class.getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ExtendedVessel source, DataIdentifier sourceIdentifier, VesselsToTonnage sourceData, ExtendedVessel target, DataIdentifier targetIdentifier, VesselsToTonnage targetData) {
		final String firstTonnageType = sourceData.getTypeId();
		final String secondTonnageType = targetData.getTypeId();

		final Float firstTonnage = sourceData.getValue();
		final Float secondTonnage = targetData.getValue();

		final boolean equalTonnageTypes = firstTonnageType != null && secondTonnageType != null && firstTonnageType.equals(secondTonnageType);
		final boolean equalTonnageValues = equalTonnageTypes && firstTonnage != null && secondTonnage != null && Double.compare(firstTonnage, secondTonnage) == 0; 
		
		if(firstTonnageType == null && secondTonnageType == null)
			return MatchingScore.getNonPerformedTemplate();
		
		if(firstTonnageType == null || secondTonnageType == null)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();

		if(firstTonnage == null && secondTonnage == null)
			return MatchingScore.getNonPerformedTemplate();
		
		if(firstTonnage == null || secondTonnage == null)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();
		
		if(equalTonnageTypes && equalTonnageValues)		
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();
		
		if(equalTonnageTypes || this._excludeTypesComparison)
			return new MatchingScore(Math.min(firstTonnage, secondTonnage) / Math.max(firstTonnage, secondTonnage), MatchingType.NON_AUTHORITATIVE);

		return MatchingScore.getNonAuthoritativeNoMatchTemplate();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.uniform.extras.matchlets.skeleton.historical.UHistoricalMatchletSkeleton#extractData(java.io.Serializable, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected Collection<VesselsToTonnage> extractData(ExtendedVessel entity, DataIdentifier dataIdentifier) {
		return entity.getTonnageData();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.MatchletSkeleton#doValidateConfiguration()
	 */
	@Override
	protected void doValidateConfiguration() throws MatchletConfigurationException {
		return;
	}
}