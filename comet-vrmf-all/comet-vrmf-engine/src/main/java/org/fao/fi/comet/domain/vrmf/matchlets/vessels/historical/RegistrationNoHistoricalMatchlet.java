/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.historical;

import java.util.Collection;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletData;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleRangeFrom;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.domain.vrmf.matchlets.vessels.support.RegistrationNoHelper;
import org.fao.fi.sh.utility.topology.helpers.TransitiveMappedDataManager;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SPorts;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Jul 2010
 */
@MatchletDefaultSerializationExclusionPolicy(MatchingSerializationExclusionPolicy.NON_PERFORMED)
@MatchletData(dataType={ VesselsToRegistrations.class })
final public class RegistrationNoHistoricalMatchlet extends AbstractUHistoricalMatchlet<ExtendedVessel, VesselsToRegistrations> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5051549998242880045L;
	
	/** The Registration Number helper reference */
	private RegistrationNoHelper _regNoHelper = new RegistrationNoHelper();

	@MatchletParameter(name="portWeight")
	@DoubleRangeFrom(value=0D, include=true)
	private double _portWeight = 10D;
	
	@MatchletParameter(name="portCountryWeight")
	@DoubleRangeFrom(value=0D, include=true)
	private double _portCountryWeight = 5D;
		
	@MatchletParameter(name="registrationNumberWeight")
	@DoubleRangeFrom(value=0D, include=true)
	private double _registrationNumberWeight = 25D;
	
	@MatchletParameter(name="countriesMapper")
	transient private TransitiveMappedDataManager<Integer, SCountries> _countriesMapper;

	@MatchletParameter(name="portsMapper")
	transient private TransitiveMappedDataManager<Integer, SPorts> _portsMapper;
		
	/**
	 * @return the 'portWeight' value
	 */
	public double getPortWeight() {
		return this._portWeight;
	}

	/**
	 * @param portWeight the 'portWeight' value to set
	 */
	public void setPortWeight(double portWeight) {
		this._portWeight = portWeight;
	}

	/**
	 * @return the 'portCountryWeight' value
	 */
	public double getPortCountryWeight() {
		return this._portCountryWeight;
	}

	/**
	 * @param portCountryWeight the 'portCountryWeight' value to set
	 */
	public void setPortCountryWeight(double portCountryWeight) {
		this._portCountryWeight = portCountryWeight;
	}

	/**
	 * @return the 'registrationNumberWeight' value
	 */
	public double getRegistrationNumberWeight() {
		return this._registrationNumberWeight;
	}

	/**
	 * @param registrationNumberWeight the 'registrationNumberWeight' value to set
	 */
	public void setRegistrationNumberWeight(double registrationNumberWeight) {
		this._registrationNumberWeight = registrationNumberWeight;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return RegistrationNoHistoricalMatchlet.class.getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ExtendedVessel source, DataIdentifier sourceIdentifier, VesselsToRegistrations sourceData, ExtendedVessel target, DataIdentifier targetIdentifier, VesselsToRegistrations targetData) {
		return this._regNoHelper.computeLikeliness(this._countriesMapper,
												   this._portsMapper,
												   this._portCountryWeight, 
												   this._portWeight,
												   this._registrationNumberWeight,
												   sourceData, 
												   targetData);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UVectorialMatchletSkeleton#doExtractData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected Collection<VesselsToRegistrations> doExtractData(ExtendedVessel entity, DataIdentifier dataIdentifier) {
		return entity.getRegistrationData();
	}
}