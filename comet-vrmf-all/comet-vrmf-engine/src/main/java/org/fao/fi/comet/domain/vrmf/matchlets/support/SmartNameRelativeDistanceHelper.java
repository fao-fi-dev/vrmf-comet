/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine-core)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.support;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nNeg;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.sh.model.core.spi.ComplexNameAware;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Jul 2010
 */
final public class SmartNameRelativeDistanceHelper<DATA extends ComplexNameAware> extends AbstractStringComparisonHelper {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 694838026317431162L;

	/**
	 * Computes the likeliness of two named data
	 * 
	 * @param simplifiedNameWeight how much should the simplified name matching (when available) weight
	 * @param firstData the first simplified named data
	 * @param secondData the second simplified named data
	 * 
	 * @return the likeliness of the name attributes of the two provided data
	 */
	public MatchingScore computeLikeliness(double nameWeight, double simplifiedNameWeight, double soundexWeight, DATA firstData, DATA secondData) {
		$nNeg(nameWeight, "The name weight cannot be lower than zero");
		$nNeg(simplifiedNameWeight, "The simplified name weight cannot be lower than zero");
		$nNeg(soundexWeight, "The soundex weight cannot be lower than zero");

		String firstName = firstData.getName();
		String secondName = secondData.getName();

		String simplifiedFirstName = firstData.getSimplifiedName();
		String simplifiedSecondName = secondData.getSimplifiedName();
		
		String firstNameSoundex = firstData.getSimplifiedNameSoundex();
		String secondNameSoundex = secondData.getSimplifiedNameSoundex();

		if(firstName == null && secondName == null)
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();
		
		if(firstName == null || secondName == null)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();

		//Sets the initial total weight to the name weight
		double totalWeight = nameWeight;
		double lambda = MatchingScore.SCORE_NO_MATCH;
		
		if(firstName.equalsIgnoreCase(secondName))
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();

		//Lambda is the weighted result of the first name and second name comparison
		lambda = nameWeight * super.compareStrings(firstName, secondName, MOVE_NUMBERS_TO_RIGHT, REMOVE_ALL_SPACES).getValue();
		
		//Simplified names do exist and can be compared
		if(simplifiedFirstName != null && simplifiedSecondName != null) {
			
			//Increases the total weight by the simplified name weight
			totalWeight += simplifiedNameWeight;

			if(simplifiedFirstName.equalsIgnoreCase(simplifiedSecondName))
				return new MatchingScore((lambda + MatchingScore.SCORE_FULL_MATCH * simplifiedNameWeight) / totalWeight, MatchingType.NON_AUTHORITATIVE);
	
			//Lambda gets increased by the weighted result of the simplified first name and simplified second name comparison
			lambda += simplifiedNameWeight * super.compareStrings(simplifiedFirstName, simplifiedSecondName, MOVE_NUMBERS_TO_RIGHT, REMOVE_ALL_SPACES).getValue();
	
			//Increases the total weight with the soundex weight
			totalWeight += soundexWeight;
			
			if(firstNameSoundex.equalsIgnoreCase(secondNameSoundex))
				return new MatchingScore((lambda + MatchingScore.SCORE_FULL_MATCH * soundexWeight) / totalWeight, MatchingType.NON_AUTHORITATIVE);
				
			Set<String> firstNameSoundexParts = new TreeSet<String>(Arrays.asList(firstNameSoundex.split("\\s")));
			Set<String> secondNameSoundexParts = new TreeSet<String>(Arrays.asList(secondNameSoundex.split("\\s"))); 
	
			int partMatchings = 0;
			
			for(String firstPart : firstNameSoundexParts)
				if(secondNameSoundexParts.contains(firstPart))
					partMatchings++;
			
			lambda += soundexWeight * ( partMatchings * 1D / firstNameSoundexParts.size() );
		}
		
		return new MatchingScore(totalWeight == 0 ? MatchingScore.SCORE_NO_MATCH : lambda / totalWeight, MatchingType.NON_AUTHORITATIVE);
	}
}