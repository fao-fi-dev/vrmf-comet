/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.support;

import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.domain.vrmf.matchlets.support.AbstractStringComparisonHelper;
import org.fao.fi.vrmf.common.models.generated.VesselsToMmsi;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Jan 2011
 */
public final class MMSIHelper extends AbstractStringComparisonHelper {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6997956737635575046L;

	public MatchingScore computeLikeliness(VesselsToMmsi firstData, VesselsToMmsi secondData) {
		return this.compareStrings(firstData.getMmsi(), secondData.getMmsi(), DONT_MOVE_NUMBERS_TO_RIGHT, REMOVE_ALL_SPACES);
	}
}