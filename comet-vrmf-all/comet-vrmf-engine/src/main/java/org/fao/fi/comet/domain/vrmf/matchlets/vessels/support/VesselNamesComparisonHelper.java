/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.support;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nNeg;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.comet.domain.vrmf.matchlets.support.AbstractStringComparisonHelper;
import org.fao.fi.sh.model.core.spi.ComplexNameAware;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Feb 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Feb 2011
 */
public class VesselNamesComparisonHelper<DATA extends ComplexNameAware> extends AbstractStringComparisonHelper {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -365347032519022002L;

	public MatchingScore computeLikeliness(double simplifiedNameWeight, double soundexWeight, DATA firstData, DATA secondData) {
		$nNeg(simplifiedNameWeight, "The simplified name weight cannot be lower than zero");
		$nNeg(soundexWeight, "The soundex weight cannot be lower than zero");

		String firstName = firstData.getName();
		String secondName = secondData.getName();
		
		String simplifiedFirstName = firstData.getSimplifiedName();
		String simplifiedSecondName = secondData.getSimplifiedName();
		
		String firstNameSoundex = firstData.getSimplifiedNameSoundex();
		String secondNameSoundex = secondData.getSimplifiedNameSoundex();
		
		if(firstName == null && secondName == null)
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();
		
		if(firstName == null || secondName == null)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();

		//Sets the initial total weight to the name weight
		double totalWeight = 0;
		double lambda = MatchingScore.SCORE_NO_MATCH;
		
		if(firstName.equalsIgnoreCase(secondName))
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();

		//Simplified names do exist and can be compared
		if(simplifiedFirstName != null && simplifiedSecondName != null) {
//			Pattern numberPattern = Pattern.compile("^(.*)\\b([0-9]+)$");
//			
			String firstNumber, secondNumber;
			
			firstNumber = secondNumber = null;
//			
//			java.util.regex.Matcher firstMatcher = numberPattern.matcher(simplifiedFirstName);
//			java.util.regex.Matcher secondMatcher = numberPattern.matcher(simplifiedSecondName);
//			
//			if(firstMatcher.matches()) {
//				firstNumber = StringHelper.rawTrim(firstMatcher.group(2));
//			}
//			
//			if(secondMatcher.matches()) {
//				secondNumber = StringHelper.rawTrim(secondMatcher.group(2));
//			}
			
			firstNumber = simplifiedFirstName.replaceAll("[^0-9]", "");
			secondNumber = simplifiedSecondName.replaceAll("[^0-9]", "");
			
			if(firstNumber != null && secondNumber != null && !firstNumber.equals(secondNumber))
				return MatchingScore.getNonAuthoritativeNoMatchTemplate();				
			
			//Increases the total weight by the simplified name weight
			totalWeight += simplifiedNameWeight;

			if(simplifiedFirstName.equalsIgnoreCase(simplifiedSecondName))
				return new MatchingScore((lambda + MatchingScore.SCORE_FULL_MATCH * simplifiedNameWeight) / totalWeight, MatchingType.NON_AUTHORITATIVE);
				
			MatchingScore simplifiedLikeliness = super.compareStrings(simplifiedFirstName, simplifiedSecondName, DONT_MOVE_NUMBERS_TO_RIGHT, REMOVE_ALL_SPACES);
			
			//Lambda gets increased by the weighted result of the simplified first name and simplified second name comparison
			lambda += simplifiedNameWeight * simplifiedLikeliness.getValue();
	
			if(!simplifiedLikeliness.isFullMatch()) {
				//Increases the total weight with the soundex weight
				totalWeight += soundexWeight;
				
				if(firstNameSoundex == null || secondNameSoundex == null)
					return new MatchingScore((lambda + MatchingScore.SCORE_NO_MATCH * soundexWeight) / totalWeight, MatchingType.NON_AUTHORITATIVE);
				
				if((firstNameSoundex == null && secondNameSoundex == null) || firstNameSoundex.equalsIgnoreCase(secondNameSoundex))
					return new MatchingScore((lambda + MatchingScore.SCORE_FULL_MATCH * soundexWeight) / totalWeight, MatchingType.NON_AUTHORITATIVE);
				
				Set<String> firstNameSoundexParts = new TreeSet<String>(Arrays.asList(firstNameSoundex.split("\\s")));
				Set<String> secondNameSoundexParts = new TreeSet<String>(Arrays.asList(secondNameSoundex.split("\\s"))); 
		
				int minParts = Math.min(firstNameSoundexParts.size(), secondNameSoundexParts.size());
					
				firstNameSoundexParts.retainAll(secondNameSoundexParts);
				
				if(firstNameSoundexParts.size() > 0) {
					if(minParts > 0)
						lambda += soundexWeight * ( firstNameSoundexParts.size() * 1D / minParts );
				}
			}
		}
		
		return new MatchingScore(totalWeight == 0 ? MatchingScore.SCORE_NO_MATCH : lambda / totalWeight, MatchingType.NON_AUTHORITATIVE);
	}
}