/**
 * (c) 2014 FAO / UN (project: comet-vrmf-engine)
 */
package org.fao.fi.comet.domain.vrmf.engine;

import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.uniform.engine.UMatchingEngineCore;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Aug 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Aug 2014
 */
public class VesselsMatchingEngine extends UMatchingEngineCore<ExtendedVessel, MatchingEngineProcessConfiguration> {
	public VesselsMatchingEngine() {
		super();
	}
}
