/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.support;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$pos;

import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.domain.vrmf.matchlets.support.AbstractStringComparisonHelper;
import org.fao.fi.sh.utility.lexical.processors.impl.SymbolsRemoverProcessor;
import org.fao.fi.sh.utility.topology.helpers.TransitiveMappedDataManager;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jul 2010
 */
final public class IRCSHelper extends AbstractStringComparisonHelper {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1428270917088610226L;
	
	static final private SymbolsRemoverProcessor SYMBOLS_REMOVER_PROCESSOR = new SymbolsRemoverProcessor();

	/**
	 * Class constructor
	 */
	public IRCSHelper() {
		super();
	}

	public MatchingScore computeLikeliness(TransitiveMappedDataManager<Integer, SCountries> mappedDataManager, double IRCSCountryWeight, double IRCSWeight, VesselsToCallsigns firstData, VesselsToCallsigns secondData) {
		$pos(IRCSCountryWeight + IRCSWeight, "The IRCS country and value weights sum should be greater than zero");
		
		double totalWeight = 0D;
		
		Integer firstCountry = firstData.getCountryId();
		Integer secondCountry = secondData.getCountryId();
		
		String firstIRCS = firstData.getCallsignId();
		String secondIRCS = secondData.getCallsignId();

		boolean bothCountriesSet = firstCountry != null && secondCountry != null;
		boolean sameCountry = bothCountriesSet && 
							( firstCountry.equals(secondCountry) ||
							  ( mappedDataManager !=null && mappedDataManager.maps(firstCountry, secondCountry) ) );
		
		totalWeight += IRCSCountryWeight;
		
		double totalLambda = MatchingScore.SCORE_NO_MATCH;
		
		if(sameCountry) {
			totalLambda += IRCSCountryWeight * MatchingScore.SCORE_FULL_MATCH;
		}
		
//		if(firstCountry == null && firstCountry == null)
//			return Likeliness.getNonPerformedTemplate();
		
//		if((firstCountry != null && secondCountry == null) ||
//		   (firstCountry == null && secondCountry != null))
//			return Likeliness.getNonAuthoritativeNoMatchTemplate();
		
//		if(!firstCountry.equals(secondCountry) &&
//		   !this._mappedCountriesManager.maps(firstCountry, secondCountry))
//			return Likeliness.getNonAuthoritativeNoMatchTemplate();

		//Now countries are either the same or are mapped one against the other
		
//		return super.compareStrings(firstIRCS, secondIRCS);

		if(firstIRCS != null)
			firstIRCS = firstIRCS.toUpperCase();
		
		if(secondIRCS != null)
			secondIRCS = secondIRCS.toUpperCase();
		
		MatchingScore IRCSLikeliness = super.compareStrings(SYMBOLS_REMOVER_PROCESSOR.process(firstIRCS), SYMBOLS_REMOVER_PROCESSOR.process(secondIRCS), DONT_MOVE_NUMBERS_TO_RIGHT, REMOVE_ALL_SPACES);
		
		totalWeight += IRCSWeight;
		
		totalLambda += IRCSWeight * IRCSLikeliness.getValue();

		IRCSLikeliness.setValue(totalLambda / totalWeight);
		
		return IRCSLikeliness;		
	}
}
