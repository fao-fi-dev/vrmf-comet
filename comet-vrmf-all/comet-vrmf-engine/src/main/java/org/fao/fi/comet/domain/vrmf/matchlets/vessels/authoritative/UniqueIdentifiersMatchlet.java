/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.authoritative;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletData;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsCutoffByDefault;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsOptionalByDefault;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.UVectorialMatchletSkeleton;
import org.fao.fi.sh.utility.model.extensions.collections.impl.SerializableArrayList;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Jul 2010
 */
@MatchletIsCutoffByDefault @MatchletIsOptionalByDefault
@MatchletData(dataType={ VesselsToIdentifiers.class })
@MatchletDefaultSerializationExclusionPolicy(MatchingSerializationExclusionPolicy.NON_PERFORMED)
public class UniqueIdentifiersMatchlet extends UVectorialMatchletSkeleton<ExtendedVessel, VesselsToIdentifiers> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2398868023020931171L;

	static final private String IMO_TYPE_ID 	= "IMO";
	static final private String EU_CFR_TYPE_ID  = "EU_CFR";
	
	static final private Set<String> UNIQUE_IDENTIFIERS_TYPE_IDS = new HashSet<String>();
	
	static {
		UNIQUE_IDENTIFIERS_TYPE_IDS.add(IMO_TYPE_ID);
		UNIQUE_IDENTIFIERS_TYPE_IDS.add(EU_CFR_TYPE_ID);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return UniqueIdentifiersMatchlet.class.getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UVectorialMatchletSkeleton#doExtractData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected Collection<VesselsToIdentifiers> doExtractData(ExtendedVessel entity, DataIdentifier dataIdentifier) {
		final List<VesselsToIdentifiers> identifiers = entity.getIdentifiers();
		
		if(identifiers == null || identifiers.size() == 0)
			return null;
		
		List<VesselsToIdentifiers> uniqueIdentifiers = new ArrayList<VesselsToIdentifiers>();
		
		for(VesselsToIdentifiers current : identifiers) {
			if(UNIQUE_IDENTIFIERS_TYPE_IDS.contains(current.getTypeId()))
				uniqueIdentifiers.add(current);
		}
		
		return new SerializableArrayList<VesselsToIdentifiers>(uniqueIdentifiers);
	}

	

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ExtendedVessel source, DataIdentifier sourceIdentifier, VesselsToIdentifiers sourceData, ExtendedVessel target, DataIdentifier targetIdentifier, VesselsToIdentifiers targetData) {
		boolean typesDiffer = !sourceData.getTypeId().equals(targetData.getTypeId()), identifiersDiffer = !typesDiffer && !sourceData.getIdentifier().equals(targetData.getIdentifier());
		
		return typesDiffer ? MatchingScore.getNonPerformedTemplate() : identifiersDiffer ? MatchingScore.getAuthoritativeNoMatchTemplate() : MatchingScore.getNonAuthoritativeFullMatchTemplate();
	}
}