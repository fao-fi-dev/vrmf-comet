/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.historical;

import java.util.Collection;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletData;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.domain.vrmf.matchlets.vessels.support.FlagHelper;
import org.fao.fi.sh.utility.topology.helpers.TransitiveMappedDataManager;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Jul 2010
 */
@MatchletDefaultSerializationExclusionPolicy(MatchingSerializationExclusionPolicy.NON_PERFORMED)
@MatchletData(dataType={ VesselsToFlags.class })
final public class FlagHistoricalMatchlet extends AbstractUHistoricalMatchlet<ExtendedVessel, VesselsToFlags> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5051549998242880045L;
	
	/** The flag helper reference */
	private FlagHelper _flagHelper = new FlagHelper();

	@MatchletParameter(name="countriesMapper")
	transient private TransitiveMappedDataManager<Integer, SCountries> _countriesMapper;
	
	/**
	 * Class constructor
	 */
	public FlagHistoricalMatchlet() {
		super();
	}

	/**
	 * @return the 'countriesMapper' value
	 */
	public final TransitiveMappedDataManager<Integer, SCountries> getCountriesMapper() {
		return this._countriesMapper;
	}

	/**
	 * @param countriesMapper the 'countriesMapper' value to set
	 */
	public final void setCountriesMapper(TransitiveMappedDataManager<Integer, SCountries> countriesMapper) {
		this._countriesMapper = countriesMapper;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return FlagHistoricalMatchlet.class.getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ExtendedVessel source, DataIdentifier sourceIdentifier, VesselsToFlags sourceData, ExtendedVessel target, DataIdentifier targetIdentifier, VesselsToFlags targetData) {
		return this._flagHelper.computeLikeliness(this._countriesMapper, sourceData, targetData);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UVectorialMatchletSkeleton#doExtractData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected Collection<VesselsToFlags> doExtractData(ExtendedVessel entity, DataIdentifier dataIdentifier) {
		return entity.getFlagData();
	}
}