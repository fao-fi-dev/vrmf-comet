/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine-core)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.historical;

import org.fao.fi.comet.core.uniform.matchlets.skeleton.UVectorialMatchletSkeleton;
import org.fao.fi.sh.model.core.spi.DateReferenced;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Feb 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 Feb 2011
 */
abstract public class AbstractUHistoricalMatchlet<ENTITY, DATA extends DateReferenced> extends UVectorialMatchletSkeleton<ENTITY, DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6826432008374557426L;
	
	protected boolean _excludeTypesComparison = false;

	/**
	 * @return the 'excludeTypesComparison' value
	 */
	final public boolean getExcludeTypesComparison() {
		return this._excludeTypesComparison;
	}

	/**
	 * @param excludeTypesComparison the 'excludeTypesComparison' value to set
	 */
	final public void setExcludeTypesComparison(boolean excludeTypesComparison) {
		this._excludeTypesComparison = excludeTypesComparison;
	}
}
