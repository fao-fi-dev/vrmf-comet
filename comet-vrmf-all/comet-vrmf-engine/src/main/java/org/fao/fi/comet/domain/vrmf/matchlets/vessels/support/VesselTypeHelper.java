/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.support;

import java.io.Serializable;

import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.sh.utility.topology.helpers.TransitiveMappedDataManager;
import org.fao.fi.vrmf.common.models.generated.SVesselTypes;
import org.fao.fi.vrmf.common.models.generated.VesselsToTypes;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jul 2010
 */
final public class VesselTypeHelper implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1859180320020044467L;
	
	public VesselTypeHelper() {
		super();
	}
	
	public MatchingScore computeLikeliness(TransitiveMappedDataManager<Integer, SVesselTypes> typesMapper, VesselsToTypes firstData, VesselsToTypes secondData) {
		Integer firstType = firstData.getTypeId();
		Integer secondType = secondData.getTypeId();

		if(firstType == null && secondType == null)
			return MatchingScore.getNonPerformedTemplate();
		
		if((firstType != null && secondType == null) ||
		   (firstType == null && secondType != null))
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();
		
		if(firstType.equals(secondType))
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();
		
		if(typesMapper != null && typesMapper.maps(firstType, secondType)) {
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();
		}

		return MatchingScore.getNonAuthoritativeNoMatchTemplate();
	}
}
