/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.authoritative;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsCutoffByDefault;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsOptionalByDefault;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton;
import org.fao.fi.sh.utility.topology.helpers.TransitiveMappedDataManager;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.Vessels;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Jul 2010
 */
@MatchletIsCutoffByDefault @MatchletIsOptionalByDefault
@MatchletDefaultSerializationExclusionPolicy(MatchingSerializationExclusionPolicy.ALWAYS)
public class MappedVesselMatchlet extends UScalarMatchletSkeleton<ExtendedVessel, Integer> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2398868023020931171L;

	@MatchletParameter(name="vesselsMapper")
	transient private TransitiveMappedDataManager<Integer, Vessels> _vesselsMapper;
	
	public MappedVesselMatchlet() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return MappedVesselMatchlet.class.getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ExtendedVessel source, DataIdentifier sourceIdentifier, Integer sourceData, ExtendedVessel target, DataIdentifier targetIdentifier, Integer targetData) {
		boolean maps = this._vesselsMapper != null ?
							this._vesselsMapper.maps(sourceData, targetData) :
							sourceData.equals(targetData);
							
		return maps ? MatchingScore.getAuthoritativeFullMatchTemplate() : MatchingScore.getNonPerformedTemplate();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton#doExtractData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected Integer doExtractData(ExtendedVessel entity, DataIdentifier dataIdentifier) {
		return entity.getId();
	}
}