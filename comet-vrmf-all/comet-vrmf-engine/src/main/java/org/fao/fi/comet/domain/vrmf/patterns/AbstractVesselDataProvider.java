/**
 * (c) 2014 FAO / UN (project: comet-vrmf-engine)
 */
package org.fao.fi.comet.domain.vrmf.patterns;

import java.util.Collection;
import java.util.Date;

import org.fao.fi.comet.domain.vrmf.common.spi.VesselDataProvider;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Aug 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Aug 2014
 */
abstract public class AbstractVesselDataProvider implements VesselDataProvider {
	protected String _providerID = this.getClass().getSimpleName() + "@" + this.hashCode();
	
	protected String[] _sources;
	protected Date _updatedFrom;
	protected Date _updatedTo;
	
	protected Collection<Class<?>> _attributes;
	
	/**
	 * Class constructor
	 */
	public AbstractVesselDataProvider() {
		this(null, null, null, null, null);
	}
	
	/**
	 * Class constructor
	 *
	 * @param providerID
	 * @param sources
	 * @param updatedFrom
	 * @param updatedTo
	 * @param attributes
	 * @throws Throwable
	 */
	public AbstractVesselDataProvider(String providerID, String[] sources, Date updatedFrom, Date updatedTo, Collection<Class<?>> attributes) {
		super();
		this._providerID = providerID;
		this._sources = sources;
		this._updatedFrom = updatedFrom;
		this._updatedTo = updatedTo;
		this._attributes = attributes;
	}

	/**
	 * Class constructor
	 *
	 * @param providerID
	 * @param sources
	 * @param updatedFrom
	 * @param updatedTo
	 * @throws Throwable
	 */
	public AbstractVesselDataProvider(String providerID, String[] sources, Date updatedFrom, Date updatedTo)  throws Throwable {
		this(providerID, sources, updatedFrom, updatedTo, null);
	}

	/**
	 * @return the 'sources' value
	 */
	public final String[] getSources() {
		return this._sources;
	}

	/**
	 * @param sources the 'sources' value to set
	 */
	public final void setSources(String[] sources) {
		this._sources = sources;
	}

	/**
	 * @return the 'updatedFrom' value
	 */
	public final Date getUpdatedFrom() {
		return this._updatedFrom;
	}

	/**
	 * @param updatedFrom the 'updatedFrom' value to set
	 */
	public final void setUpdatedFrom(Date updatedFrom) {
		this._updatedFrom = updatedFrom;
	}

	/**
	 * @return the 'updatedTo' value
	 */
	public final Date getUpdatedTo() {
		return this._updatedTo;
	}

	/**
	 * @param updatedTo the 'updatedTo' value to set
	 */
	public final void setUpdatedTo(Date updatedTo) {
		this._updatedTo = updatedTo;
	}

	/**
	 * @return the 'attributes' value
	 */
	public final Collection<Class<?>> getAttributes() {
		return this._attributes;
	}

	/**
	 * @param attributes the 'attributes' value to set
	 */
	public final void setAttributes(Collection<Class<?>> attributes) {
		this._attributes = attributes;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.DataProvider#getProviderID()
	 */
	@Override
	final public String getProviderID() {
		return this._providerID;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.DataProvider#setProviderID(java.lang.String)
	 */
	@Override
	final public void setProviderID(String providerID) {
		this._providerID = providerID;
	}

	abstract protected void initialize() throws Throwable;
}
