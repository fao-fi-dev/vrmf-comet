/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.support;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$pos;

import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.domain.vrmf.matchlets.support.AbstractStringComparisonHelper;
import org.fao.fi.sh.utility.lexical.processors.impl.SymbolsRemoverProcessor;
import org.fao.fi.sh.utility.topology.helpers.TransitiveMappedDataManager;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SPorts;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jul 2010
 */
final public class RegistrationNoHelper extends AbstractStringComparisonHelper {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -9158218894892361426L;
	
	static final private SymbolsRemoverProcessor SYMBOLS_REMOVER_PROCESSOR = new SymbolsRemoverProcessor();;
	
	/**
	 * Class constructor
	 *
	 * @param mappedPortsManager
	 */
	public RegistrationNoHelper() {
		super();
	}

	/**
	 * @param firstData
	 * @param secondData
	 * @return
	 */
	public MatchingScore computeLikeliness(TransitiveMappedDataManager<Integer, SCountries> countriesMapper, TransitiveMappedDataManager<Integer, SPorts> portsMapper, double countryWeight, double portWeight, double numberWeight, VesselsToRegistrations firstData, VesselsToRegistrations secondData) {
		$pos(countryWeight + portWeight + numberWeight, "The Reg. No. country, port and value weights sum should be greater than zero");
		
		double totalWeight = 0D;
		
		String firstRegNo = firstData.getRegistrationNumber();
		String secondRegNo = secondData.getRegistrationNumber();

		double totalLambda = MatchingScore.SCORE_NO_MATCH;

		if(portWeight > 0D) {
			Integer firstPortID = firstData.getPortId();
			Integer secondPortID = secondData.getPortId();
	
			boolean bothPortsSet = firstPortID != null && secondPortID != null;
			
			boolean samePort = bothPortsSet && 
							 ( firstPortID.equals(secondPortID) || 
								( portsMapper != null && portsMapper.maps(firstPortID, secondPortID) ) );
						
			totalWeight += bothPortsSet ? portWeight : 0;
				
			if(samePort) {
				totalLambda += portWeight * MatchingScore.SCORE_FULL_MATCH;
			} 
		} 
		
		if(countryWeight > 0D) {
			Integer firstCountryID = firstData.getCountryId();
			Integer secondCountryID = secondData.getCountryId();
	
			boolean bothCountriesSet = firstCountryID != null && secondCountryID != null;
			
			boolean sameCountry = bothCountriesSet && 
								( firstCountryID.equals(secondCountryID) || 
									( countriesMapper != null && countriesMapper.maps(firstCountryID, secondCountryID) ) );
						
			totalWeight += bothCountriesSet ? countryWeight : 0;
				
			if(sameCountry) {
				totalLambda += countryWeight * MatchingScore.SCORE_FULL_MATCH;
			} 
		}

		MatchingScore registrationNumberLikeliness = super.compareStrings(SYMBOLS_REMOVER_PROCESSOR.process(firstRegNo), SYMBOLS_REMOVER_PROCESSOR.process(secondRegNo), DONT_MOVE_NUMBERS_TO_RIGHT, REMOVE_ALL_SPACES);
		
		totalWeight += numberWeight;
		
		totalLambda += numberWeight * registrationNumberLikeliness.getValue();

		registrationNumberLikeliness.setValue(totalLambda / totalWeight);
		
		return registrationNumberLikeliness;
	}
}
