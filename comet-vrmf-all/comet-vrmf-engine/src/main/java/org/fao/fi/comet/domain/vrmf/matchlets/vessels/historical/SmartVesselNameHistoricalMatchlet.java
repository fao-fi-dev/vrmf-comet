/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.historical;

import java.util.Collection;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletData;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleRangeFrom;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.domain.vrmf.matchlets.vessels.support.VesselNamesComparisonHelper;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Jul 2010
 */
@MatchletDefaultSerializationExclusionPolicy(MatchingSerializationExclusionPolicy.NON_PERFORMED)
@MatchletData(dataType={ VesselsToName.class })
final public class SmartVesselNameHistoricalMatchlet extends AbstractUHistoricalMatchlet<ExtendedVessel, VesselsToName> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5051549998242880045L;
	
	/** The smart name helper reference */
	private VesselNamesComparisonHelper<VesselsToName> _nameHelper = new VesselNamesComparisonHelper<VesselsToName>();
	
	/** The value to weight the simplified name matching when needed */
	@MatchletParameter(name="simplifiedNameWeight")
	@DoubleRangeFrom(value=0D, include=true)
	private double _simplifiedNameWeight = 10D;

	/** The value to weight the simplified name soundex matching when needed */
	@MatchletParameter(name="soundexWeight")
	@DoubleRangeFrom(value=0D, include=true)
	private double _soundexWeight = 5D;

	/**
	 * Class constructor
	 */
	public SmartVesselNameHistoricalMatchlet() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return SmartVesselNameHistoricalMatchlet.class.getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ExtendedVessel source, DataIdentifier sourceIdentifier, VesselsToName sourceData, ExtendedVessel target, DataIdentifier targetIdentifier, VesselsToName targetData) {
		return this._nameHelper.computeLikeliness(//this._nameWeight,
												  this._simplifiedNameWeight,
												  this._soundexWeight,
												  sourceData, 
												  targetData);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UVectorialMatchletSkeleton#doExtractData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected Collection<VesselsToName> doExtractData(ExtendedVessel entity, DataIdentifier dataIdentifier) {
		return entity.getNameData();
	}
}