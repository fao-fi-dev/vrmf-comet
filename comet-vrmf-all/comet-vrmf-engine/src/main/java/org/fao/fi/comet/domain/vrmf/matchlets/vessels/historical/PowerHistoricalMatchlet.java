/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.historical;

import java.util.Collection;

import org.fao.fi.comet.core.exceptions.MatchletConfigurationException;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletData;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.comet.extras.uniform.matchlets.skeleton.historical.UTypedValuedHistoricalMatchlet;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToPower;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Jul 2010
 */
@MatchletDefaultSerializationExclusionPolicy(MatchingSerializationExclusionPolicy.NON_PERFORMED)
@MatchletData(dataType={ VesselsToPower.class })
final public class PowerHistoricalMatchlet extends UTypedValuedHistoricalMatchlet<ExtendedVessel, VesselsToPower> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5051549998242880045L;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return PowerHistoricalMatchlet.class.getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ExtendedVessel source, DataIdentifier sourceIdentifier, VesselsToPower sourceData, ExtendedVessel target, DataIdentifier targetIdentifier, VesselsToPower targetData) {
		final String firstPowerType = sourceData.getTypeId();
		final String secondPowerType = targetData.getTypeId();
		
		final Boolean firstMainEngine = sourceData.getMainEngine();
		final Boolean secondMainEngine = targetData.getMainEngine();

		final Float firstPower = sourceData.getValue();
		final Float secondPower = targetData.getValue();

		final boolean equalPowerTypes = firstPowerType != null && secondPowerType != null && firstPowerType.equals(secondPowerType);
		final boolean equalEngineTypes = equalPowerTypes && firstMainEngine != null && secondMainEngine != null && firstMainEngine.equals(secondMainEngine);
		final boolean equalPowerValues = equalEngineTypes && firstPower != null && secondPower != null && Double.compare(firstPower, secondPower) == 0;

		if(firstPowerType == null && secondPowerType == null)
			return MatchingScore.getNonPerformedTemplate();

		if(firstMainEngine == null && secondMainEngine == null)
			return MatchingScore.getNonPerformedTemplate();
		
		if(firstPower == null && secondPower == null)
			return MatchingScore.getNonPerformedTemplate();
				
		if(firstPowerType == null || secondPowerType == null)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();

		if(firstMainEngine == null || secondMainEngine == null)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();
		
		if(firstPower == null || secondPower == null)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();

		if(equalPowerValues)		
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();
		
		if((equalPowerTypes && equalEngineTypes) || this._excludeTypesComparison)
			return new MatchingScore(Math.min(firstPower, secondPower) / Math.max(firstPower, secondPower), MatchingType.NON_AUTHORITATIVE);
		
		return MatchingScore.getNonAuthoritativeNoMatchTemplate();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.uniform.extras.matchlets.skeleton.historical.UHistoricalMatchletSkeleton#extractData(java.io.Serializable, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected Collection<VesselsToPower> extractData(ExtendedVessel entity, DataIdentifier dataIdentifier) {
		return entity.getPowerData();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.MatchletSkeleton#doValidateConfiguration()
	 */
	@Override
	protected void doValidateConfiguration() throws MatchletConfigurationException {
		return;
	}
}