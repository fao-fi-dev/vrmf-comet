/**
 * (c) 2014 FAO / UN (project: comet-vrmf-engine)
 */
package org.fao.fi.comet.domain.vrmf.patterns.support;

import java.util.Date;
import java.util.Iterator;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Aug 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Aug 2014
 */
abstract public class AbstractVesselIDFilteringIterator implements Iterator<Integer> {
	private Iterator<Integer> _availableIDsIterator;
	
	/**
	 * Class constructor
	 *
	 * @param sources
	 * @param updatedFrom
	 * @param updatedTo
	 */
	public AbstractVesselIDFilteringIterator(String[] sources, Date updatedFrom, Date updatedTo) throws Throwable {
		super();
		
		this._availableIDsIterator = this.initialize(sources, updatedFrom, updatedTo);
	}
	
	abstract protected Iterator<Integer> initialize(String[] sources, Date updatedFrom, Date updatedTo) throws Throwable;

	/* (non-Javadoc)
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {
		return this._availableIDsIterator == null ? false : this._availableIDsIterator.hasNext();
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#next()
	 */
	@Override
	public Integer next() {
		return this._availableIDsIterator == null ? null : this._availableIDsIterator.next();
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		if(this._availableIDsIterator != null)
			this._availableIDsIterator.remove();
	}
}
