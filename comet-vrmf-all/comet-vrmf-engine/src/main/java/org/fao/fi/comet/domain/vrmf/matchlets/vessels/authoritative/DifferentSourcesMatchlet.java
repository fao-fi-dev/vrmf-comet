/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-comparison-engine)
 */
package org.fao.fi.comet.domain.vrmf.matchlets.vessels.authoritative;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsCutoffByDefault;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsOptionalByDefault;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Jun 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Jun 2012
 */
@MatchletIsCutoffByDefault @MatchletIsOptionalByDefault
@MatchletDefaultSerializationExclusionPolicy(MatchingSerializationExclusionPolicy.ALWAYS)
public class DifferentSourcesMatchlet extends UScalarMatchletSkeleton<ExtendedVessel, String> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5843053517938490043L;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return DifferentSourcesMatchlet.class.getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ExtendedVessel source, DataIdentifier sourceIdentifier, String sourceData, ExtendedVessel target, DataIdentifier targetIdentifier, String targetData) {
		return sourceData.equals(targetData) ? 
				MatchingScore.getAuthoritativeNoMatchTemplate() : 
				MatchingScore.getNonPerformedTemplate();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton#doExtractData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected String doExtractData(ExtendedVessel entity, DataIdentifier dataIdentifier) {
		return entity.getSourceSystem();
	}
}
