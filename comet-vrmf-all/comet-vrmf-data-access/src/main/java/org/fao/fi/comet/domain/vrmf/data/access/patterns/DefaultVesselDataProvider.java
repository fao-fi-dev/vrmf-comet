/**
 * (c) 2014 FAO / UN (project: comet-vrmf-engine)
 */
package org.fao.fi.comet.domain.vrmf.data.access.patterns;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.inject.Inject;
import javax.inject.Named;

import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.comet.domain.vrmf.data.access.dao.VesselIDFilteringDAO;
import org.fao.fi.comet.domain.vrmf.patterns.AbstractVesselDataProvider;
import org.fao.fi.vrmf.business.dao.ExtendedVesselsDAO;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Aug 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Aug 2014
 */
@Named("default.vessel.data.provider")
public class DefaultVesselDataProvider extends AbstractVesselDataProvider {
	@Inject transient private ExtendedVesselsDAO _extendedVesselsDAO;
	@Inject transient private VesselIDFilteringDAO _vesselIDFilteringDAO;

	/**
	 * Class constructor
	 *
	 */
	public DefaultVesselDataProvider() {
		super();
		
		this.setProviderID(this.getClass().getSimpleName() + "@" + this.hashCode());
		
		DataProviderDelegate.resetCache();
	}

	/**
	 * Class constructor
	 *
	 * @param providerID
	 * @param sources
	 * @param updatedFrom
	 * @param updatedTo
	 * @param attributes
	 */
	public DefaultVesselDataProvider(String providerID, String[] sources, Date updatedFrom, Date updatedTo, Collection<Class<?>> attributes) {
		super(providerID, sources, updatedFrom, updatedTo, attributes);
	}

	/**
	 * Class constructor
	 *
	 * @param providerID
	 * @param sources
	 * @param updatedFrom
	 * @param updatedTo
	 * @throws Throwable
	 */
	public DefaultVesselDataProvider(String providerID, String[] sources, Date updatedFrom, Date updatedTo) throws Throwable {
		super(providerID, sources, updatedFrom, updatedTo);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.patterns.AbstractVesselDataProvider#initialize()
	 */
	@Override
	protected void initialize() throws Throwable {
	}

	/**
	 * @return the 'extendedVesselsDAO' value
	 */
	public ExtendedVesselsDAO getExtendedVesselsDAO() {
		return this._extendedVesselsDAO;
	}

	/**
	 * @param extendedVesselsDAO the 'extendedVesselsDAO' value to set
	 */
	public void setExtendedVesselsDAO(ExtendedVesselsDAO extendedVesselsDAO) {
		this._extendedVesselsDAO = extendedVesselsDAO;
	}

	/**
	 * @return the 'vesselIDFilteringDAO' value
	 */
	public final VesselIDFilteringDAO getVesselIDFilteringDAO() {
		return this._vesselIDFilteringDAO;
	}

	/**
	 * @param vesselIDFilteringDAO the 'vesselIDFilteringDAO' value to set
	 */
	public final void setVesselIDFilteringDAO(VesselIDFilteringDAO vesselIDFilteringDAO) {
		this._vesselIDFilteringDAO = vesselIDFilteringDAO;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.DataProvider#releaseResources()
	 */
	@Override
	public void releaseResources() throws Exception {
		
	}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<ProvidedData<ExtendedVessel>> iterator() {
		try {
			return new IteratorDelegate(this._providerID, this.getIDs().iterator(), new DataProviderDelegate(this._extendedVesselsDAO, this._attributes));
		} catch(Throwable t) {
			throw new RuntimeException("Unable to return data iterator: " + t.getMessage(), t);
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider#getAvailableDataSize()
	 */
	@Override
	public int getAvailableDataSize() {
		try {
			return this.getIDs().size();
		} catch (Throwable t) {
			throw new RuntimeException("Unable to determine available data size: " + t.getMessage(), t);
		}
	}
	
	private Collection<Integer> getIDs() throws Throwable {
		Collection<Integer> IDs = this._vesselIDFilteringDAO.filter(this._sources, this._updatedFrom, this._updatedTo);
		
		return IDs == null ? new ArrayList<Integer>() : IDs;
	}
}
