/**
 * (c) 2014 FAO / UN (project: comet-vrmf-data-access)
 */
package org.fao.fi.comet.domain.vrmf.data.access.patterns;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.fao.fi.vrmf.business.dao.ExtendedVesselsDAO;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Aug 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Aug 2014
 */
public class DataProviderDelegate {
	private ExtendedVesselsDAO _extendedVesselsDAO;
	private Collection<Class<?>> _attributes;
	
	static private Map<Integer, ExtendedVessel> CACHE = new HashMap<Integer, ExtendedVessel>();
	
	/**
	 * Class constructor
	 *
	 * @param extendedVesselsDAO
	 */
	public DataProviderDelegate(ExtendedVesselsDAO extendedVesselsDAO, Collection<Class<?>> attributes) {
		super();
		
		this._extendedVesselsDAO = extendedVesselsDAO;
		this._attributes = attributes;
	}

	public ExtendedVessel getByID(Integer ID) throws Throwable {
		if(!CACHE.containsKey(ID)) {
			CACHE.put(ID, this._extendedVesselsDAO.selectExtendedByPrimaryKey(ID, this._attributes == null ? null : this._attributes));
		}
			
		return CACHE.get(ID); 
	}
	
	static public void resetCache() {
		CACHE.clear();
	}
}
