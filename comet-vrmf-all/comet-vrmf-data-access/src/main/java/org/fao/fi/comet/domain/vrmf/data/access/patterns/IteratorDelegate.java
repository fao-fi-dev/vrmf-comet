/**
 * (c) 2014 FAO / UN (project: comet-vrmf-data-access)
 */
package org.fao.fi.comet.domain.vrmf.data.access.patterns;

import java.util.Iterator;

import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Aug 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Aug 2014
 */
public class IteratorDelegate implements Iterator<ProvidedData<ExtendedVessel>> {
	private String _providerID;
	private Iterator<Integer> _IDIterator;
	private DataProviderDelegate _providerDelegate;
	
	/**
	 * Class constructor
	 *
	 * @param providerID
	 * @param IDIterator
	 * @param providerDelegate
	 */
	public IteratorDelegate(String providerID, Iterator<Integer> IDIterator, DataProviderDelegate providerDelegate) {
		super();
		
		this._providerID = providerID;
		this._IDIterator = IDIterator;
		this._providerDelegate = providerDelegate;
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {
		return this._IDIterator.hasNext();
	}
	
	/* (non-Javadoc)
	 * @see java.util.Iterator#next()
	 */
	@Override
	public ProvidedData<ExtendedVessel> next() {
		Integer currentID = null;
		
		try {
			currentID = this._IDIterator.next();
			
			ExtendedVessel data = this._providerDelegate.getByID(currentID);
			
			return new ProvidedData<ExtendedVessel>(this._providerID, data);
		} catch (Throwable t) {
			throw new RuntimeException("Unable to retrieve data by ID #" + currentID + ": " + t.getMessage(), t); 
		}
	}
	
	/* (non-Javadoc)
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		this._IDIterator.remove();
	}
}