/**
 * (c) 2014 FAO / UN (project: comet-vrmf-data-access)
 */
package org.fao.fi.comet.domain.vrmf.data.access.dao.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.comet.domain.vrmf.data.access.dao.VesselIDFilteringDAO;
import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractConstructorInjectionIBATISDAO;
import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractIBATISDAO;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.annotations.IBATISNamespace;

import com.ibatis.sqlmap.client.SqlMapClient;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Aug 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Aug 2014
 */
@Named @Singleton @IBATISNamespace(defaultNamespacePrefix="vesselIDFilter")
public class VesselIDFilteringDAOImpl extends AbstractConstructorInjectionIBATISDAO implements VesselIDFilteringDAO {
	/**
	 * Class constructor
	 *
	 * @param client
	 */
	@Inject
	@Named(AbstractIBATISDAO.VRMF_DEFAULT_SQL_MAP_CLIENT_BEAN_ID)
	public VesselIDFilteringDAOImpl(SqlMapClient client) {
		super(client);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.data.access.dao.VesselIDFilteringDAO#filter(java.lang.String[], java.util.Date, java.util.Date)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Integer> filter(String[] sources, Date updatedFrom, Date updatedTo) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sources", sources);
		params.put("updatedFrom", updatedFrom);
		params.put("updatedTo", updatedTo);
		
		return this.getSqlMapClient().queryForList(this.getQueryIDForCurrentNamespace("filter"), params);
	}
}
