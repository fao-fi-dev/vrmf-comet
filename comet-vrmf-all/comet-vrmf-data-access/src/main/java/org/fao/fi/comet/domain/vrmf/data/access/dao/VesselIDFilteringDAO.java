/**
 * (c) 2014 FAO / UN (project: comet-vrmf-data-access)
 */
package org.fao.fi.comet.domain.vrmf.data.access.dao;

import java.util.Collection;
import java.util.Date;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Aug 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Aug 2014
 */
public interface VesselIDFilteringDAO {
	Collection<Integer> filter(String[] sources, Date updatedFrom, Date updatedTo) throws Exception;
}
