/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-vrmf-engine-server)
 */
package org.fao.fi.comet.domain.vrmf.server.rest.support;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * @author Fiorellato
 *
 */
@Target({ ElementType.TYPE, ElementType.FIELD })
public @interface CustomConverter {

}
