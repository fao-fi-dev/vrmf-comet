/**
 * 
 */
package org.fao.fi.comet.domain.vrmf.server.rest;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * @author Fiorellato
 *
 */
public class Application extends ResourceConfig {
	/**
	 * Register JAX-RS application components.
	 */
	public Application() {
		register(JerseyMatchingService.class);
	}
}