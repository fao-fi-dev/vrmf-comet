/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-vrmf-engine-server)
 */
package org.fao.fi.comet.domain.vrmf.server.rest;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;

import org.fao.fi.comet.core.exceptions.MatchingProcessException;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus;
import org.fao.fi.comet.core.model.engine.MatchingEngineStatus;
import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;
import org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver;
import org.fao.fi.comet.domain.vrmf.gui.remote.response.CheckEngineStatusResponse;
import org.fao.fi.comet.domain.vrmf.gui.remote.response.CountVesselsResponse;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Fiorellato
 *
 */
@Named @Singleton @RequestMapping("matching/*")
public class SpringMatchingService {
	@Inject private @Singleton @Named("matching.engine.driver.local") MatchingEngineDriver _localDriver;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#countTargets(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@RequestMapping(value="count/sources", method=RequestMethod.POST, consumes={MediaType.APPLICATION_FORM_URLENCODED}, produces={MediaType.APPLICATION_XML}) 
	@ResponseBody
	public CountVesselsResponse countSources(@FormParam(value="configuration") String configuration) {
		try {
			return new CountVesselsResponse(this._localDriver.countSources(JAXBHelper.fromXML(MatchingProcessConfiguration.class, configuration)));
		} catch(Throwable t) {
			throw new BadRequestException(t);
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#countTargets(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@RequestMapping(value="count/targets", method=RequestMethod.POST, consumes={MediaType.APPLICATION_FORM_URLENCODED}, produces={MediaType.APPLICATION_XML}) 
	@ResponseBody
	public CountVesselsResponse countTargets(@FormParam(value="configuration") String configuration) {
		try {
			return new CountVesselsResponse(this._localDriver.countTargets(JAXBHelper.fromXML(MatchingProcessConfiguration.class, configuration)));
		} catch(Throwable t) {
			throw new BadRequestException(t);
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#getProcessStatus()
	 */
	@RequestMapping(value="engine/status", method=RequestMethod.GET, produces={MediaType.APPLICATION_XML}) 
	@ResponseBody
	public MatchingEngineProcessStatus getProcessStatus() {
		return this._localDriver.getProcessStatus();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#isProcessRunning()
	 */
	@RequestMapping(value="engine/running", method=RequestMethod.GET, produces={MediaType.APPLICATION_XML}) 
	@ResponseBody
	public CheckEngineStatusResponse isProcessRunning() {
		return new CheckEngineStatusResponse(this._localDriver.getProcessStatus().is(MatchingEngineStatus.RUNNING));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#startProcess(org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration)
	 */
	@RequestMapping(value="engine/match", method=RequestMethod.POST, consumes={MediaType.APPLICATION_FORM_URLENCODED}, produces={MediaType.APPLICATION_XML}) 
	@ResponseBody
	public String startProcess(@FormParam(value="configuration") String configuration) throws MatchingProcessException {
		System.out.println(">> SERVER : startProcess - BEGIN");
		try {
			MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> result = this._localDriver.startProcess(JAXBHelper.fromXML(MatchingProcessConfiguration.class, configuration));
			
			System.out.println(">> SERVER : startProcess - COMPLETE");
			
			return JAXBHelper.toXML(
				new Class[] { 
					VesselsToIdentifiers.class,
					VesselsToFlags.class,
					VesselsToName.class,
					VesselsToCallsigns.class,
					VesselsToRegistrations.class,
					VesselsToLengths.class,
					VesselsToTonnage.class,
					ExtendedVessel.class
				}, result);
		} catch(Throwable t) {
			t.printStackTrace();
			
			throw new BadRequestException(t);
		} finally {
			System.out.println(">> SERVER : startProcess - END");
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#haltProcess()
	 */
	@RequestMapping(value="engine/halt", method=RequestMethod.DELETE) 
	public void haltProcess() {
		this._localDriver.haltProcess();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#getProcessConfiguration()
	 */
	@RequestMapping(value="engine/configuration", method=RequestMethod.GET, produces={MediaType.APPLICATION_XML}) 
	@ResponseBody
	public MatchingProcessConfiguration getProcessConfiguration() {
		return this._localDriver.getProcessConfiguration();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#dump()
	 */
	@RequestMapping(value="engine/dump", method=RequestMethod.GET, produces={MediaType.APPLICATION_XML}) 
	@ResponseBody
	public String dump() throws Exception {
		MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> result = this._localDriver.dump();
		
		return JAXBHelper.toXML(
			new Class[] { 
				VesselsToIdentifiers.class,
				VesselsToFlags.class,
				VesselsToName.class,
				VesselsToCallsigns.class,
				VesselsToRegistrations.class,
				VesselsToLengths.class,
				VesselsToTonnage.class,
				ExtendedVessel.class
			}, result);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver#getRefreshTime()
	 */
	public int getRefreshTime() {
		return -1;
	}
}
