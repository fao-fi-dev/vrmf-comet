/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-vrmf-engine-server)
 */
package org.fao.fi.comet.domain.vrmf.server.rest.support;

import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;
import org.springframework.core.convert.converter.Converter;

/**
 * @author Fiorellato
 *
 */
public class MatchingEngineProcessResultConverter implements Converter<String, MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration>> {
	@SuppressWarnings("unchecked")
	@Override
	public MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> convert(String source) {
		try {
			return JAXBHelper.fromXML(
				MatchingEngineProcessResult.class, 
				new Class[] {
					VesselsToIdentifiers.class,
					VesselsToFlags.class,
					VesselsToName.class,
					VesselsToCallsigns.class,
					VesselsToRegistrations.class,
					VesselsToLengths.class,
					VesselsToTonnage.class,
					ExtendedVessel.class
				},
				source
			);
		} catch (Throwable t) {
			throw new IllegalArgumentException("Unable to convert Matching Engine Process Result from provided parameter", t);
		}
	}
}
