/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-vrmf-engine-server)
 */
package org.fao.fi.comet.domain.vrmf.server.rest;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

import org.fao.fi.comet.core.exceptions.MatchingProcessException;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus;
import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;
import org.fao.fi.comet.domain.vrmf.common.spi.MatchingEngineDriver;
import org.fao.fi.comet.domain.vrmf.gui.remote.response.CountVesselsResponse;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;

/**
 * @author Fiorellato
 *
 */
@Named @Singleton @Path("matching")
public class JerseyMatchingService {
	@Inject private @Singleton @Named("matching.engine.driver.local") MatchingEngineDriver _localDriver;
	
	@POST @Path("count/sources") @Produces(MediaType.APPLICATION_XML) @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public CountVesselsResponse countSources(@FormParam(value="configuration") String configuration) {
		try {
			return new CountVesselsResponse(this._localDriver.countSources(JAXBHelper.fromXML(MatchingProcessConfiguration.class, configuration)));
		} catch(Throwable t) {
			throw new BadRequestException(t);
		}
	}

	@POST @Path("count/targets") @Produces(MediaType.APPLICATION_XML) @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public CountVesselsResponse countTargets(@FormParam(value="configuration") String configuration) {
		try {
			return new CountVesselsResponse(this._localDriver.countTargets(JAXBHelper.fromXML(MatchingProcessConfiguration.class, configuration)));
		} catch(Throwable t) {
			throw new BadRequestException(t);
		}
	}

	@GET @Path("engine/status") @Produces(MediaType.APPLICATION_XML)
	public MatchingEngineProcessStatus getProcessStatus() {
		return this._localDriver.getProcessStatus();
	}
	
	@POST @Path("engine/match/async") @Consumes(MediaType.APPLICATION_FORM_URLENCODED) @Produces(MediaType.APPLICATION_XML)
	public void startProcess(@Suspended final AsyncResponse asyncResponse, final @FormParam(value="configuration") String configuration) throws MatchingProcessException {
		final JerseyMatchingService $this = this;
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println(">> SERVER : startProcess - BEGIN");
				try {
					MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> result = $this._localDriver
							.startProcess(JAXBHelper.fromXML(MatchingProcessConfiguration.class, configuration));

					System.out.println(">> SERVER : startProcess - COMPLETE");

					asyncResponse.resume(
						JAXBHelper.toXML(
							new Class[] {
								VesselsToIdentifiers.class,
								VesselsToFlags.class,
								VesselsToName.class,
								VesselsToCallsigns.class,
								VesselsToRegistrations.class,
								VesselsToLengths.class,
								VesselsToTonnage.class,
								ExtendedVessel.class 
							}, 
							result
						)
					);
				} catch (Throwable t) {
					t.printStackTrace();

					throw new BadRequestException(t);
				} finally {
					System.out.println(">> SERVER : startProcess - END");
				}
			}
		}).start();
	}
	
	@POST @Path("engine/match") @Consumes(MediaType.APPLICATION_FORM_URLENCODED) @Produces(MediaType.APPLICATION_XML)
	public String startProcess(@FormParam(value="configuration") String configuration) throws MatchingProcessException {
		System.out.println(">> SERVER : startProcess - BEGIN");
		try {
			MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> result = this._localDriver.startProcess(JAXBHelper.fromXML(MatchingProcessConfiguration.class, configuration));

			System.out.println(">> SERVER : startProcess - COMPLETE");

			return 
				JAXBHelper.toXML(
					new Class[] {
						VesselsToIdentifiers.class,
						VesselsToFlags.class,
						VesselsToName.class,
						VesselsToCallsigns.class,
						VesselsToRegistrations.class,
						VesselsToLengths.class,
						VesselsToTonnage.class,
						ExtendedVessel.class 
					}, 
					result
				);
		} catch (Throwable t) {
			t.printStackTrace();

			throw new BadRequestException(t);
		} finally {
			System.out.println(">> SERVER : startProcess - END");
		}
	}


	@DELETE @Path("engine/halt")
	public void haltProcess() {
		this._localDriver.haltProcess();
	}

	@GET @Path("engine/configuration") @Produces(MediaType.APPLICATION_XML)
	public MatchingProcessConfiguration getProcessConfiguration() {
		return this._localDriver.getProcessConfiguration();
	}

	@GET @Path("engine/dump") @Produces(MediaType.APPLICATION_XML)
	public String dump() throws Exception {
		MatchingEngineProcessResult<ExtendedVessel, ExtendedVessel, MatchingEngineProcessConfiguration> result = this._localDriver.dump();
		
		return JAXBHelper.toXML(
			new Class[] { 
				VesselsToIdentifiers.class,
				VesselsToFlags.class,
				VesselsToName.class,
				VesselsToCallsigns.class,
				VesselsToRegistrations.class,
				VesselsToLengths.class,
				VesselsToTonnage.class,
				ExtendedVessel.class
			}, result);
	}
}
