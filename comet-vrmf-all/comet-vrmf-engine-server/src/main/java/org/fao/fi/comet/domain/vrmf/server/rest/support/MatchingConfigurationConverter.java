/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-vrmf-engine-server)
 */
package org.fao.fi.comet.domain.vrmf.server.rest.support;

import org.fao.fi.comet.domain.vrmf.common.MatchingProcessConfiguration;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author Fiorellato
 *
 */
@Component
@CustomConverter
public class MatchingConfigurationConverter implements Converter<String, MatchingProcessConfiguration> {
	@Override
	public MatchingProcessConfiguration convert(String source) {
		try {
			return JAXBHelper.fromXML(
				MatchingProcessConfiguration.class, 
				source
			);
		} catch (Throwable t) {
			throw new IllegalArgumentException("Unable to convert Matching Process Configuration from provided parameter", t);
		}
	}
}
