/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-vrmf-engine-server)
 */
package org.fao.fi.comet.domain.vrmf.server.rest.support;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;

/**
 * @author Fiorellato
 *
 */
public class ConversionServiceFactoryBean extends FormattingConversionServiceFactoryBean {
	@Resource
	@CustomConverter
	private List<Converter<?, ?>> _customConverters;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void installFormatters(final FormatterRegistry registry) {
		super.installFormatters(registry);

		for (final Converter<?, ?> converter : this._customConverters) {
			registry.addConverter(converter);
		}
	}
}
